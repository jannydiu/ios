//
//  ExploreTableViewController.swift
//  MyBooks
//
//  Created by Jan on 2019/4/13.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class ExploreTableViewController: UITableViewController,UISearchBarDelegate {

    var searchBar = UISearchBar()
    var searchTerm: String = "apple"
    var allBooks: [Book] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = searchBar
        searchBar.placeholder = "search"
        self.navigationController?.navigationBar.barTintColor = .yellow
        searchBar.delegate = self
        // book service to get books
        updateData()
    }

    //ref: https://developer.apple.com/documentation/uikit/uisearchbar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //ref: https://stackoverflow.com/questions/35480530/swift-remove-white-spaces-from-string-doesnt-work
        let searchtext = searchBar.text!
        let puretext = searchtext.components(separatedBy: .whitespaces)
        self.searchTerm = puretext.joined()
        updateData()
    }
    
    
    func updateData() {
        BookService.search(for: self.searchTerm, completion: { books, error in
            guard let books = books, error == nil else{
                print(error ?? "unknown error")
                // alart
                let alert = UIAlertController(title: "Error", message: "Please check your network and search term :(", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                }))
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.allBooks = books
            self.tableView.reloadData()
        })
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allBooks.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "explorecell", for: indexPath) as? ExploreTableViewCell else { return UITableViewCell()}
        let book = allBooks[indexPath.row]
        cell.titleLabel.text = book.title
        cell.authorLabel.text = book.author
        return cell
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailVC = segue.destination as? ExploreDetailViewController else { return }
        guard let selectedIndexPath = tableView.indexPathForSelectedRow else { return }
        let book = allBooks[selectedIndexPath.row]
        detailVC.book = book
    }
}
