//
//  MyBooksTableViewController.swift
//  MyBooks
//
//  Created by Jan on 2019/4/15.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class MyBooksTableViewController: UITableViewController {
    
    var bookRecords: [BookRecord] = []
    var wantToRead: [BookRecord] = []
    var read: [BookRecord] = []
    let service = CloudKitService.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = .yellow
    }

    override func viewWillAppear(_ animated: Bool) {
        updateData()
    }
    
    func updateData() {
        self.read = []
        self.wantToRead = []
        service.fetchAll { (items, error) in
            guard let items = items, error == nil else {
                print(error ?? "unknown error")
                return
            }
            DispatchQueue.main.async {
                self.bookRecords = items
                for record in self.bookRecords {
                    if record.read {
                        self.read.append(record)
                    } else {
                        self.wantToRead.append(record)
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    //ref: https://developer.apple.com/documentation/uikit/uitableviewdatasource
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "WantToRead"
        case 1:
            return "Read"
        default:
            return ""
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return wantToRead.count
        } else {
            return read.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "mybookscell", for: indexPath) as? MyBooksTableViewCell else { return UITableViewCell()}
        // Configure the cell...
        if indexPath.section == 0 {
            let book = wantToRead[indexPath.row].book
            cell.titleLabel.text = book.title
            cell.authorLabel.text = book.author
        } else {
            let book = read[indexPath.row].book
            cell.titleLabel.text = book.title
            cell.authorLabel.text = book.author
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if indexPath.section == 0 {
                let record = self.wantToRead[indexPath.row]
                service.delete(item: record)
                self.wantToRead.remove(at: indexPath.row)
            } else {
                let record = self.read[indexPath.row]
                service.delete(item: record)
                self.read.remove(at: indexPath.row)
            }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailVC = segue.destination as? MyBooksDetailViewController else { return }
        guard let selectedIndexPath = tableView.indexPathForSelectedRow else { return }
        if selectedIndexPath.section == 0 {
            let record = wantToRead[selectedIndexPath.row]
            detailVC.bookRecord = record
            detailVC.isRead = false
        } else {
            let record = read[selectedIndexPath.row]
            detailVC.bookRecord = record
            detailVC.isRead = true
        }
    }
}
