//
//  MyBooksDetailViewController.swift
//  MyBooks
//
//  Created by Jan on 2019/4/15.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class MyBooksDetailViewController: UIViewController, UITextViewDelegate {

    @IBOutlet var labels: [UILabel]!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var myNotes: UITextView!
    
    var book: Book!
    var isRead: Bool!
    var bookRecord: BookRecord!
    let service = CloudKitService.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //ref: https://stackoverflow.com/questions/10077155/how-to-add-done-button-to-the-keyboard
        myNotes.returnKeyType = .done
        myNotes.delegate = self
        
        if isRead {
            let wantToReadButton = UIBarButtonItem(title: "WantToRead", style: .plain, target: self, action: #selector(saveToWantToRead))
            self.navigationItem.setRightBarButton(wantToReadButton, animated: true)
        } else {
            let readButton = UIBarButtonItem(title: "Read", style: .plain, target: self, action: #selector(saveToRead))
            self.navigationItem.setRightBarButton(readButton, animated: true)
        }
        
        book = bookRecord.book
        labels[0].text = "Title: " + (book?.title ?? "None")
        labels[1].text = "Author:" + (book?.author ?? "None")
        // for date
        let dt = book.publishedDate
        let endIndex = dt.index(dt.startIndex, offsetBy: 10)
        labels[2].text = "PublishedDate:" + ( dt[dt.startIndex..<endIndex] )
        labels[3].text = "Price:$" + (book.price.description )
        // ref:https://stackoverflow.com/questions/25827033/how-do-i-convert-a-swift-array-to-a-string
        labels[4].text = "Genres:" + (book.genres.joined(separator: "-") )
        labels[5].text = "AverageUserRating:" + (book.averageUserRating.description)
        labels[6].text = "UserRatingCount:" + (book.userRatingCount.description)
        descriptionText.text = book.description
        if bookRecord.record["myNotes"] == "" {
            myNotes.text = "Add notes here if you want :)"
        } else {
            myNotes.text = bookRecord.record["myNotes"]
        }
    }
    
    //ref: https://stackoverflow.com/questions/26600359/dismiss-keyboard-with-a-uitextview
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            bookRecord.record["myNotes"] = myNotes.text
            service.save(item: bookRecord)
            return false
        }
        return true
    }
    
    @objc func saveToRead() {
        bookRecord.record["read"] = true
        service.save(item: bookRecord)
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @objc func saveToWantToRead() {
        bookRecord.record["read"] = false
        service.save(item: bookRecord)
        self.navigationItem.rightBarButtonItem = nil
    }
}
