//
//  ViewController.swift
//  MyBooks
//
//  Created by Jan on 2019/4/13.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class ExploreDetailViewController: UIViewController {
    
    var book: Book!
    var books: [Book] = []
    var bookRecords: [BookRecord] = []
    let service = CloudKitService.shared
    
    @IBOutlet var labels: [UILabel]!
    @IBOutlet weak var dscrpt: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        service.fetchAll { (items, error) in
            guard let items = items, error == nil else {
                print(error ?? "unknown error")
                return
            }
            DispatchQueue.main.async {
                self.bookRecords = items
                for record in self.bookRecords {
                    self.books.append(record.book)
                }
                if self.books.count == 0 || !self.books.contains(where: {$0.trackId == self.book.trackId}) {
                    let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(self.saveToWantToRead))
                    self.navigationItem.setRightBarButton(saveButton, animated: true)
                }
            }
        }
        
        labels[0].text = "Title: " + (book?.title ?? "None")
        labels[1].text = "Author:" + (book?.author ?? "None")
        // for date
        let dt = book.publishedDate
        let endIndex = dt.index(dt.startIndex, offsetBy: 10)
        labels[2].text = "PublishedDate:" + ( dt[dt.startIndex..<endIndex] )
        labels[3].text = "Price:$" + (book.price.description )
        // ref:https://stackoverflow.com/questions/25827033/how-do-i-convert-a-swift-array-to-a-string
        labels[4].text = "Genres:" + (book.genres.joined(separator: "-") )
        labels[5].text = "AverageUserRating:" + (book.averageUserRating.description)
        labels[6].text = "UserRatingCount:" + (book.userRatingCount.description)
        dscrpt.text = book.description
        
    }
    
    @objc func saveToWantToRead() {
        self.navigationItem.rightBarButtonItem = nil
        service.save(item: BookRecord(book: book, read: false))
    }
}

