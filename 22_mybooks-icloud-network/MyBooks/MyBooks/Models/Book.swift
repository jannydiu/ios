//
//  File.swift
//  MyBooks
//
//  Created by Jan on 2019/4/14.
//  Copyright © 2019 Jan. All rights reserved.
//

import CloudKit

struct BooksResult: Codable {
    let resultCount: Int
    let results: [Book]
}

struct Book: Codable {
    var title: String
    var author: String
    var publishedDate: String
    var price: Float
    var genres: [String]
    var description: String
    var averageUserRating: Float
    var userRatingCount: Int
    var trackId: Int
    
    enum CodingKeys: String, CodingKey {
        case title = "trackName"
        case author = "artistName"
        case publishedDate = "releaseDate"
        
        case price
        case genres
        case description
        case averageUserRating
        case userRatingCount
        case trackId
    }
}

class BookService {
    private static let urlString = "https://itunes.apple.com/search?country=US&media=ebook&limit=20&term="
    static func search(for searchTerm:String, completion: @escaping ([Book]?, Error?) -> ()) {
        guard let url = URL(string: urlString + searchTerm) else {
            print("invalid url: \(urlString + searchTerm)")
            return
        }
        let urlRequest = URLRequest(url: url)
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForResource = 3
        config.allowsCellularAccess = false
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: urlRequest) { data,response,error in
            guard let data = data, let response = response as? HTTPURLResponse, error == nil else {
                DispatchQueue.main.async { completion(nil, error) }
                return
            }
            print("Status code: \(response.statusCode)")
            do {
                let decoder = JSONDecoder()
                let booksResult = try decoder.decode(BooksResult.self, from: data)
                // what does this mean?
                DispatchQueue.main.async { completion(booksResult.results, nil) }
            } catch (let error) {
                DispatchQueue.main.async { completion(nil, error) }
            }
        }
        task.resume()
    }
}

class BookRecord {
    var book: Book
    var record: CKRecord
    var read: Bool
    
    init(book: Book, read: Bool) {
        self.book = book
        self.read = read
        let record = CKRecord(recordType: "Book")
        record["title"] = book.title
        record["author"] = book.author
        record["publishedDate"] = book.publishedDate
        record["price"] = book.price
        record["genres"] = book.genres
        record["description"] = book.description
        record["averageUserRating"] = book.averageUserRating
        record["userRatingCount"] = book.userRatingCount
        record["trackId"] = book.trackId
        record["read"] = read
        record["myNotes"] = ""
        self.record = record
    }
    
    init?(record: CKRecord) {
        self.record = record
        guard let title = record["title"] as? String else { return nil }
        guard let author = record["author"] as? String else { return nil }
        guard let publishedDate = record["publishedDate"] as? String else { return nil }
        guard let price = record["price"] as? Float else { return nil }
        guard let genres = record["genres"] as? [String] else { return nil }
        guard let description = record["description"] as? String else { return nil }
        guard let averageUserRating = record["averageUserRating"] as? Float else { return nil }
        guard let userRatingCount = record["userRatingCount"] as? Int else { return nil }
        guard let trackId = record["trackId"] as? Int else { return nil }
        guard let read = record["read"] as? Bool else { return nil }
        
        self.book = Book(title: title, author: author, publishedDate: publishedDate, price: price, genres: genres, description: description, averageUserRating: averageUserRating, userRatingCount: userRatingCount, trackId: trackId)
        self.read = read
        
    }
}
