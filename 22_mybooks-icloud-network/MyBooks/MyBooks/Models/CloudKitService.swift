//
//  CloudKitService.swift
//  MyBooks
//
//  Created by Jan on 2019/4/21.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit
import CloudKit

class CloudKitService {
    
    static let shared = CloudKitService()
    private let database = CKContainer.default().privateCloudDatabase
    // to make it a singleton
    private init() {}
    
    func save(item: BookRecord) {
        database.save(item.record) { (record, error) in
            guard let record = record,error == nil else {
                print(error ?? "error saving item")
                return
            }
            print("saved record \(record)")
        }
    }
    
    func delete(item: BookRecord) {
        database.delete(withRecordID: item.record.recordID) { (recordID, error) in
            guard let recordID = recordID, error == nil else {
                print(error ?? "error deleting item")
                return
            }
            print("deleted record \(recordID)")
        }
    }
    
    func fetchAll(completion: @escaping ([BookRecord]?, Error?) -> Void) {
        let query = CKQuery(recordType: "Book", predicate: NSPredicate(value: true))
        database.perform(query, inZoneWith: CKRecordZone.default().zoneID) { (records, error) in
            guard let records = records, error == nil else {
                completion(nil, error)
                return
            }
            
            let items = records.compactMap({ BookRecord(record: $0) })
            completion(items, nil)
        }
    }
}
