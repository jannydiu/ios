//
//  AppDelegate.swift
//  MyBooks
//
//  Created by Jan on 2019/4/13.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
}

