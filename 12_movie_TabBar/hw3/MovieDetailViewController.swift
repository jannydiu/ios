//
//  MovieDetailViewController.swift
//  hw3
//
//  Created by Jan on 2019/1/26.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    // MARK: properties
    var dtitlestr: String?
    var dgenrestr: String = "Genre: "
    var dratestr: String = "Rating: "
    var ddatestr: String = "Release Date: "
    var dimgvimg: UIImage?
    var ddscptstr: String?
    var preurl: String?
    var navtitle: String?
    
    
    @IBOutlet weak var dtitle: UILabel!
    
    @IBOutlet weak var dgenre: UILabel!
    
    @IBOutlet weak var drate: UILabel!
    
    @IBOutlet weak var ddate: UILabel!
    
    @IBOutlet weak var dimgv: UIImageView!
    
    @IBOutlet weak var ddscpt: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = dtitlestr
        self.dtitle.text = dtitlestr
        self.dgenre.text = dgenrestr
        self.drate.text = dratestr
        self.ddate.text = ddatestr
        self.dimgv.image = dimgvimg
        self.ddscpt.text = ddscptstr
        
        
        // Do any additional setup after loading the view.
    }
    

    // MARK: action
    
    @IBAction func preview(_ sender: Any) {
        if (preurl != nil){
            // ref: https://stackoverflow.com/questions/12416469/how-to-launch-safari-and-open-url-from-ios-app
            UIApplication.shared.open(NSURL(string:preurl!)! as URL)
        }else{
            print("No preview for \(dtitlestr ?? "default")")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

