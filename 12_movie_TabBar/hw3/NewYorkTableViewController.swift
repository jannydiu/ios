//
//  NewYorkTableViewController.swift
//  hw3
//
//  Created by Jan on 2019/1/25.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class NewYorkTableViewController: UITableViewController
{
    var movie = MovieService()
    var movies = [Movie]()
    
    
    @IBOutlet weak var refControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        movie.search(for: "newyork", completion: { movies, error in
            guard let movies = movies, error == nil else {
                print(error ?? "unknown error")
                return
            }
            print("Movies fetched: \(movies.count)")
            self.movies = movies
            self.tableView.reloadData()
        })
        
        // ref: https://stackoverflow.com/questions/24475792/how-to-use-pull-to-refresh-in-swift
        
        refControl.attributedTitle = NSAttributedString(string: "Fetching data...")
        refControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refControl)
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    @objc func refresh(_ sender: Any) {
        //  your code to refresh tableView
        movie.search(for: "newyork", completion: { movies, error in
            guard let movies = movies, error == nil else {
                print(error ?? "unknown error")
                return
            }
            print("Movies fetched: \(movies.count)")
            self.movies = movies
            self.tableView.reloadData()
        })
        self.refControl.endRefreshing()
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "nyc", for: indexPath) as? MovieTableViewCell else{
            return UITableViewCell()
        }
        let movie = movies[indexPath.row]
        cell.titleLabel.text = movie.trackName
        cell.genreLabel.text = movie.primaryGenreName
        if movie.hasITunesExtras ?? false {
            cell.imgv.image = UIImage(named:"popcorn")
        }else{
            cell.imgv.image = UIImage(named: "ticket")
        }
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        guard let detailViewController = segue.destination as? MovieDetailViewController else{
            return
        }
        
        // Pass the selected object to the new view controller.
        guard let selectedCell = sender as? MovieTableViewCell else{
            return
        }
        guard let selectIndex = tableView.indexPath(for: selectedCell) else{
            return
        }
        let movie = movies[selectIndex.row]
        
        detailViewController.dtitlestr = movie.trackName
        detailViewController.dgenrestr += movie.primaryGenreName!
        detailViewController.dratestr += movie.contentAdvisoryRating!
        // time
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let dated = formatter.date(from: movie.releaseDate!)
        formatter.dateStyle = .medium
        let dates = formatter.string(from: dated!)
        detailViewController.ddatestr += dates
        
        // time format: detailViewController.ddate.text =
        if movie.hasITunesExtras ?? false {
            detailViewController.dimgvimg = UIImage(named:"popcorn")
        }else{
            detailViewController.dimgvimg = UIImage(named: "ticket")
        }
        
        detailViewController.ddscptstr = movie.longDescription
        
        // navigation back button
        
        //detailViewController.navtitle = "NYC Movies"
        let backItem = UIBarButtonItem()
        backItem.title = "NYC Movies"
        navigationItem.backBarButtonItem = backItem
        
        
    }
    
    
}
