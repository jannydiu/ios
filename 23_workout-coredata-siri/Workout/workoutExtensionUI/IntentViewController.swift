//
//  IntentViewController.swift
//  workoutExtensionUI
//
//  Created by Janny Wang on 5/5/19.
//  Copyright © 2019 Jan. All rights reserved.
//

import IntentsUI
import CoreData

class IntentViewController: UIViewController, INUIHostedViewControlling {
    
    var persistentContainer: NSPersistentContainer! = CoreDataHelper.shared.persistentContainer
    
    @IBOutlet weak var label: UILabel!

    func configure(with interaction: INInteraction,
                       context: INUIHostedViewContext,
                       completion: @escaping (CGSize) -> Void) {
        
        let intent = interaction.intent
        let count = UserDefaults(suiteName: "group.WorkoutSiri")!.integer(forKey: "count")
        let name = UserDefaults(suiteName: "group.WorkoutSiri")!.string(forKey: "name")!
        if interaction.intentHandlingStatus == .failure {
            completion(CGSize.zero)
            return
        } else {
            if intent is INStartWorkoutIntent {
                self.label.text = "Starting your \(count) \(name)"
                completion(CGSize(width: 300, height: 150))
            } else if intent is INEndWorkoutIntent {
                self.label.text = "Ending your \(count) \(name)"
                completion(CGSize(width: 300, height: 150))
            }
        }
        
 
    }
}
