//
//  model.swift
//  Workout
//
//  Created by Jan on 2019/5/8.
//  Copyright © 2019 Jan. All rights reserved.
//

import Foundation

class MyFormatter {
    
    static func getDate(type: String) -> String {
        let time = Date()
        let formatter = DateFormatter()
        switch type {
        case "date":
            formatter.dateFormat = "MM/dd/yyyy"
        case "time":
            formatter.dateFormat = "MM/dd/yyyy HH : mm:ss"
        default:
            formatter.dateFormat = "MM/dd/yyyy HH : mm:ss"
        }
        return formatter.string(from: time)
    }
    
    static func startFormat(start: String, end: String?) -> String {
        if let end = end {
            let text = "Start: " + String(String(start.dropFirst(11)).dropLast(3)) + "\n" + "End: " + end
            return text
        } else {
            let text = "Start: " + String(String(start.dropFirst(11)).dropLast(3))
            return text
        }
    }
}

