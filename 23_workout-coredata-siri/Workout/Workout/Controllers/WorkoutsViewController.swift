//
//  WorkoutsViewController.swift
//  Workout
//
//  Created by Jan on 2019/4/30.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit
import CoreData
import Intents


class WorkoutsViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    //ref: https://www.youtube.com/watch?v=kecV6xPTTr8&feature=youtu.be&list=PL0dzCUj1L5JHfozquTVhV4HRy-1A_aXlv

    @IBOutlet weak var allWorkoutscv: UICollectionView!
    @IBOutlet weak var addButton: UIButton!
    
    var persistentContainer: NSPersistentContainer! = CoreDataHelper.shared.persistentContainer
    private var fetchedResultsController: NSFetchedResultsController<Category>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        allWorkoutscv.delegate = self
        allWorkoutscv.dataSource = self
        // Get the NSManagedObjectContext
        let context = persistentContainer.viewContext
        let request = NSFetchRequest<Category>(entityName: "Category")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        // Create the NSFetchedResultsController
        fetchedResultsController = NSFetchedResultsController(
            fetchRequest: request,
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        // Set the view controller as the delegate
        fetchedResultsController?.delegate = self
        // Fetch the results
        do {
            try fetchedResultsController?.performFetch()
        } catch {
            print("fetch request failed")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Ask user for permission to interact with Siri
        INPreferences.requestSiriAuthorization { status in
            print(status)
        }
    }
    
    
    //ref: https://cocoacasts.com/how-do-unwind-segues-work
    @IBAction func unwind(segue: UIStoryboardSegue) {
        guard let source = segue.source as? AddWorkoutViewController else { return }
        if let name = source.name {
            saveWorkout(withName: name)
        }
    }
    
    func saveWorkout(withName name: String) {
        // Get the NSManagedObjectContext
        let context = persistentContainer.viewContext
        // Tell the context to execute a block of code
        context.perform {
            // Create an NSManagedObject instance
            let workout = Category(context: context)
            workout.name = name
            do {
                // Save the context
                try context.save()
            } catch {
                // Rollback changes if needed
                context.rollback()
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedResultsController?.fetchedObjects?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = allWorkoutscv.dequeueReusableCell(withReuseIdentifier: "workoutcell", for: indexPath) as? WorkoutCollectionViewCell else {
            return UICollectionViewCell()
        }
        let workout = fetchedResultsController?.object(at: indexPath)
        cell.workoutLabel.text = workout?.name
        return cell
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        guard let destVC = segue.destination as? AworkoutViewController else { return }
        //ref: https://stackoverflow.com/questions/26314852/how-to-get-a-selected-item-in-collection-view-using-indexpathsforselecteditems
        let cell = sender as! WorkoutCollectionViewCell
        guard let selectedIndexPath = allWorkoutscv.indexPath(for: cell) else { return }
        guard let workout = fetchedResultsController?.object(at: selectedIndexPath) else { return }
        destVC.persistentContainer = persistentContainer
        destVC.workout = workout

    }
    

}

extension WorkoutsViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        
        guard type == .insert, let insertIndexPath = newIndexPath else { return }
        allWorkoutscv.insertItems(at: [insertIndexPath])
    }
}
