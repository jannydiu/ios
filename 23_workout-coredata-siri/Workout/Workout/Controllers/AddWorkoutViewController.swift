//
//  AddWorkoutViewController.swift
//  Workout
//
//  Created by Jan on 2019/4/30.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class AddWorkoutViewController: UIViewController {

    @IBOutlet weak var newWorkoutText: UITextField!
    
    @IBOutlet weak var saveButton: UIButton!
    
    var name: String? {
        return newWorkoutText.text
    }

}
