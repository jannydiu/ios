//
//  ArecordViewController.swift
//  Workout
//
//  Created by Jan on 2019/4/30.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class ArecordViewController: UIViewController {

    @IBOutlet weak var startLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var imagev: UIImageView!
    
    @IBOutlet weak var endButton: UIButton!
    
    var end: String {
        let time = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH : mm"
        return formatter.string(from: time)
    }
    
    var date: String!
    var start: String!
    var status: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.navigationItem.title = date
        if (start.contains("End")) {
            startLabel.text = start
        } else {
            let substart = "Start: " + String(String(start.dropLast(3)).dropFirst(11))
            startLabel.text = substart
        }
        
        statusLabel.text = status
        if (status == Optional("Workout completed ")) {
            endButton.isEnabled = false
            endButton.isHidden = true
        }
        
    }

}
