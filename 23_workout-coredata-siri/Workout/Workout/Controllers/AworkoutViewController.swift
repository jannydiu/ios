//
//  AworkoutViewController.swift
//  Workout
//
//  Created by Jan on 2019/4/30.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit
import CoreData

class AworkoutViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var recordstv: UITableView!
    
    // from the previou controller
    var persistentContainer: NSPersistentContainer!
    var workout: Category!
    private var records: [Workout] {
        get {return Array(workout.workout as! Set<Workout>)}
    }
    var inProgress: Bool! = UserDefaults(suiteName: "group.WorkoutSiri")!.bool(forKey: "inProgress")
    var fixed: [Workout]!
    private var fetchedResultsController: NSFetchedResultsController<Workout>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recordstv.delegate = self
        recordstv.dataSource = self
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.managedObjectContextDidChange(notification:)),
            name: .NSManagedObjectContextObjectsDidChange,
            object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        recordstv.reloadData()
    }
    
    @objc func managedObjectContextDidChange(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        if let inserts = userInfo[NSInsertedObjectsKey] as? Set<Workout> {
            print("insert",inserts.count)
        } else if let updates = userInfo[NSUpdatedObjectsKey] as? Set<Workout> {
            print("update",updates.count)
        }
        recordstv.reloadData()
    }

    private func saveNewRecord(withStart: String, withDate: String) {
        // Get the NSManagedObjectContext
        let context = persistentContainer.viewContext
        
        // Tell the context to execute a block of code
        context.perform {
            // Create an NSManagedObject instance
            let record = Workout(context: context)
            record.start = withStart
            record.date = withDate
            let records: Set<AnyHashable>? = self.workout.workout?.adding(record)
            self.workout.workout = NSSet(set: records ?? [record])
            UserDefaults(suiteName: "group.WorkoutSiri")!.set(records?.count ?? 1, forKey: "count")
            UserDefaults(suiteName: "group.WorkoutSiri")!.set(self.workout.name, forKey: "name")
            do {
                try context.save()
                self.recordstv.reloadData()
            } catch {
                context.rollback()
            }
        }
    }
    
    @IBAction func unwind(segue: UIStoryboardSegue) {
        guard let source = segue.source as? ArecordViewController else { return }
        update(start: source.start, withEnd: source.end)
    }
    
    func update(start: String, withEnd: String) {
        inProgress = false
        UserDefaults(suiteName: "group.WorkoutSiri")!.set(inProgress, forKey: "inProgress")
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Workout>(entityName: "Workout")
        let time = start
        fetchRequest.predicate = NSPredicate(format:"start == %@",time)
        context.perform {
            do {
                let test = try context.fetch(fetchRequest)
                let obj = test[0] as NSManagedObject
                obj.setValue(withEnd, forKey: "end")
                try context.save()
                self.recordstv.reloadData()
            } catch {
                context.rollback()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = recordstv.dequeueReusableCell(withIdentifier: "recordcell", for: indexPath) as? RecordTableViewCell else {
            return UITableViewCell()
        }
        // configure the cell here
        let record = records[indexPath.row]
        cell.dateLabel.text = record.date
        cell.startLabel.text = MyFormatter.startFormat(start: record.start!, end: nil)
        if (record.end == nil) {
            cell.endLabel.text = "Workout in progress"
        } else {
            cell.endLabel.text = "End: " + record.end!
        }
        fixed = records
        return cell
    }
 
    // MARK: - Navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         guard let destVC = segue.destination as? ArecordViewController else { return }
         if segue.identifier == "fromstarttorecord" {
            if inProgress {
                // alart
                let alert = UIAlertController(title: "Not Allowed", message: "Please end the current workout session before start a new one", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                }))
                self.present(alert, animated: true, completion: nil)
                return
            } else {
                inProgress = true
                UserDefaults(suiteName: "group.WorkoutSiri")!.set(inProgress, forKey: "inProgress")
                
                let start = MyFormatter.getDate(type: "time")
                UserDefaults(suiteName: "group.WorkoutSiri")!.set(start, forKey: "start")
                let date = MyFormatter.getDate(type: "date")
                destVC.start = start
                destVC.date = date
                destVC.status = "Workout in progress"
                saveNewRecord(withStart: start, withDate: date)
            }
         } else {
            guard let selectedIndexPath = recordstv.indexPathForSelectedRow else { return }
            let record = fixed[selectedIndexPath.row]
            destVC.date = record.date
            if (record.end == nil) {
                destVC.start = record.start!
                destVC.status = "Workout in progress"
            } else {
                destVC.start = MyFormatter.startFormat(start: record.start!, end: record.end!)
                destVC.status = "Workout completed "
            }
         }
     } 

}

