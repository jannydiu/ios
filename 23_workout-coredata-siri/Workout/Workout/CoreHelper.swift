//
//  CoreHelper.swift
//  Workout
//
//  Created by Jan on 2019/5/7.
//  Copyright © 2019 Jan. All rights reserved.
//

import CoreData

class CoreDataHelper {
    
    static let shared = CoreDataHelper()
    
    private init() {}
    
    let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Model")
        
        let containerUrl = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.WorkoutSiri")!
        let databaseUrl = containerUrl.appendingPathComponent("Model.sqlite")
        let description = NSPersistentStoreDescription(url: databaseUrl)
        container.persistentStoreDescriptions = [description]
        
        container.loadPersistentStores { _, error in
            if let error = error {
                print(error.localizedDescription)
            }
        }
        return container
    }()
}

