//
//  WorkoutTests.swift
//  WorkoutTests
//
//  Created by Jan on 2019/5/9.
//  Copyright © 2019 Jan. All rights reserved.
//

import XCTest


class WorkoutTests: XCTestCase {

    func testGetDate() {
        let date = MyFormatter.getDate(type: "date")
        XCTAssertEqual(date, "05/09/2019")
        let time = MyFormatter.getDate(type: "time").dropLast(8)
        XCTAssertEqual(time, "05/09/2019 19")
    }
    
    func testStartFormatter() {
        let start = MyFormatter.startFormat(start: "05/09/2019 19 : 20:22", end: nil)
        XCTAssertEqual(start, "Start: 19 : 20")
        let end = MyFormatter.startFormat(start: "05/09/2019 19 : 20:22", end: "19 : 50")
        XCTAssertEqual(end, "Start: 19 : 20\nEnd: 19 : 50")
    }

}
