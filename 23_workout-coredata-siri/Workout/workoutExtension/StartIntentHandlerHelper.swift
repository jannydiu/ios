//
//  StartIntentHandlerHelper.swift
//  workoutExtension
//
//  Created by Janny Wang on 5/7/19.
//  Copyright © 2019 Jan. All rights reserved.
//

import Intents
import CoreData

class StartWorkoutIntentHandler: NSObject, INStartWorkoutIntentHandling {
    

    var persistentContainer: NSPersistentContainer! = CoreDataHelper.shared.persistentContainer
    var workout: Category!
    
    private var names: [String] = []
    
    //  MARK: resolve
    func resolveWorkoutName(for intent: INStartWorkoutIntent, with completion: @escaping (INSpeakableStringResolutionResult) -> Void) {
        
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Category>(entityName: "Category")
        context.perform {
            do {
                let test = try context.fetch(fetchRequest)
                let objs = test as [Category]
                self.names = objs.map{$0.name!}
                guard let name = intent.workoutName,
                    self.names.contains(name.spokenPhrase) else {
                        completion(INSpeakableStringResolutionResult.unsupported())
                        return
                }
                completion(INSpeakableStringResolutionResult.success(with: name))
            } catch {
                print(error)
                return
            }
        }

    }
 
    func resolveIsOpenEnded(for intent: INStartWorkoutIntent, with completion: @escaping (INBooleanResolutionResult) -> Void) {
        let open = UserDefaults(suiteName: "group.WorkoutSiri")!.bool(forKey: "inProgress")
        if open {
            completion(INBooleanResolutionResult.confirmationRequired(with: open))
        } else {
            completion(INBooleanResolutionResult.success(with: open))
        }
    }
 
    
    // MARK: confirm
    func confirm(intent: INStartWorkoutIntent, completion: @escaping (INStartWorkoutIntentResponse) -> Void) {
        if intent.isOpenEnded ?? true {
            completion(INStartWorkoutIntentResponse(code: .failure, userActivity: nil))
        } else {
            completion(INStartWorkoutIntentResponse(code: .ready, userActivity: nil))
        }
    }
    
    // MARK: handle
    func handle(intent: INStartWorkoutIntent, completion: @escaping (INStartWorkoutIntentResponse) -> Void) {
        if let name = intent.workoutName {
            let context = persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<Category>(entityName: "Category")
            fetchRequest.predicate = NSPredicate(format:"name == %@",name.spokenPhrase)
            context.perform {
                do {
                    let test = try context.fetch(fetchRequest)
                    let obj = test[0] as Category
                    self.workout = obj
                    
                    let record = Workout(context: context)
                    let start = MyFormatter.getDate(type: "time")
                    UserDefaults(suiteName: "group.WorkoutSiri")!.set(start, forKey: "start")
                    UserDefaults(suiteName: "group.WorkoutSiri")!.set(true, forKey: "inProgress")
                    let date = MyFormatter.getDate(type: "date")
                    record.start = start
                    record.date = date
                    let records: Set<AnyHashable>? = self.workout.workout?.adding(record)
                    self.workout?.workout = NSSet(set: records ?? [record])
                    UserDefaults(suiteName: "group.WorkoutSiri")!.set(records?.count ?? 1, forKey: "count")
                    UserDefaults(suiteName: "group.WorkoutSiri")!.set(name.spokenPhrase, forKey: "name")
                    try context.save()
                    completion(INStartWorkoutIntentResponse(code:.handleInApp, userActivity: nil))
                } catch {
                    context.rollback()
                    completion(INStartWorkoutIntentResponse(code: .failure, userActivity: nil))
                    return
                }
            }
        }
    }
}

