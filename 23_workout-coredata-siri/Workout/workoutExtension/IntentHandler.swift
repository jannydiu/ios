//
//  IntentHandler.swift
//  workoutExtension
//
//  Created by Janny Wang on 5/5/19.
//  Copyright © 2019 Jan. All rights reserved.
//

import Intents

class IntentHandler: INExtension{

    override func handler(for intent: INIntent) -> Any? {
        if intent is INStartWorkoutIntent {
            return StartWorkoutIntentHandler()
        } else if intent is INEndWorkoutIntent {
            return EndWorkoutIntentHandler()
        }
        return nil
    }
    
}
