//
//  EndIntentHandlerHelper.swift
//  Workout
//
//  Created by Jan on 2019/5/7.
//  Copyright © 2019 Jan. All rights reserved.
//

import Intents
import CoreData

class EndWorkoutIntentHandler: NSObject,INEndWorkoutIntentHandling {    
    
    var persistentContainer: NSPersistentContainer! = CoreDataHelper.shared.persistentContainer
    var workout: Category!
    
    private var names: [String] = []
    
    //  MARK: resolve
    func resolveWorkoutName(for intent: INEndWorkoutIntent, with completion: @escaping (INSpeakableStringResolutionResult) -> Void) {
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Category>(entityName: "Category")
        context.perform {
            do {
                let test = try context.fetch(fetchRequest)
                let objs = test as [Category]
                self.names = objs.map{$0.name!}
                guard let name = intent.workoutName,
                    self.names.contains(name.spokenPhrase) else {
                    completion(INSpeakableStringResolutionResult.unsupported())
                    return
                }
                completion(INSpeakableStringResolutionResult.success(with: name))
            } catch {
                print(error)
                return
            }
        }
    }
    
    // MARK: confirm
    func confirm(intent: INEndWorkoutIntent, completion: @escaping (INEndWorkoutIntentResponse) -> Void) {
        completion(INEndWorkoutIntentResponse(code: .ready, userActivity: nil))
    }
    // MARK: handle
    func handle(intent: INEndWorkoutIntent, completion: @escaping (INEndWorkoutIntentResponse) -> Void) {
        let inProgress = UserDefaults(suiteName: "group.WorkoutSiri")!.bool(forKey: "inProgress")
        if let name = intent.workoutName {
            let originName = UserDefaults(suiteName: "group.WorkoutSiri")!.string(forKey: "name")
            if originName != name.spokenPhrase {
                completion(INEndWorkoutIntentResponse(code: .failure, userActivity: nil))
                return
            }
        }
        if inProgress {
            UserDefaults(suiteName: "group.WorkoutSiri")!.set(false, forKey: "inProgress")
            let context = persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<Workout>(entityName: "Workout")
            let start = UserDefaults(suiteName: "group.WorkoutSiri")!.string(forKey: "start")!
            fetchRequest.predicate = NSPredicate(format:"start == %@",start)
            let time = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "HH : mm"
            let end = formatter.string(from: time)
            context.perform {
                do {
                    let test = try context.fetch(fetchRequest)
                    let obj = test[0] as NSManagedObject
                    obj.setValue(end, forKey: "end")
                    try context.save()
                    completion(INEndWorkoutIntentResponse(code: .handleInApp, userActivity: nil))
                } catch {
                    context.rollback()
                    completion(INEndWorkoutIntentResponse(code: .failure, userActivity: nil))
                }
            }
        } else {
            completion(INEndWorkoutIntentResponse(code: .failure, userActivity: nil))
        }
    }
}
