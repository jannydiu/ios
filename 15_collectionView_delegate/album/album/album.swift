//
//  album.swift
//  album
//
//  Created by Jan on 2019/2/25.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

struct AlbumResult: Codable {
    let resultCount: Int
    let results: [Album]
}

struct Album: Codable{
    let wrapperType: String
    let artistName: String?
    let collectionName: String?
    let artworkUrl100: String?
    let collectionId: Int
}


class AlbumService {
    var dataTask: URLSessionDataTask?
    
    private let urlString = "https://itunes.apple.com/search?country=US&media=music&entity=album&limit=100&term="
    
    func search(for term: String, completion: @escaping ([Album]?, Error?) -> ()) {
        
        guard let url = URL(string: urlString + term) else {
            print("invalid url: \(urlString + term)")
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data, let response = response as? HTTPURLResponse, error == nil else {
                DispatchQueue.main.async { completion(nil, error) }
                return
            }
            
            print("Status code: \(response.statusCode)")
//            print(String(data: data, encoding: .utf8) ?? "unable to print data")
            
            do {
                let decoder = JSONDecoder()
                let albumResult = try decoder.decode(AlbumResult.self, from: data)
                DispatchQueue.main.async { completion(albumResult.results, nil) }
            } catch (let error) {
                DispatchQueue.main.async { completion(nil, error) }
                
            }
        }
        task.resume()
    }
}
