//
//  DetailViewController.swift
//  album
//
//  Created by Jan on 2019/2/23.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

protocol albumDelegate: class {
    func add(_button:DetailViewController,
             didAdd: Album, atfav: IndexPath,atall:IndexPath)
    func remove(_button:DetailViewController,
                didRemove:Album, atfav:IndexPath,atall:IndexPath)
}

class DetailViewController: UIViewController {
    
    weak var delegate: albumDelegate?

    
    @IBOutlet weak var collection: UILabel!
    
    @IBOutlet weak var artist: UILabel!
    
    @IBOutlet weak var fav: UIButton!
    
    @IBOutlet weak var subv: UIView!
    
    let empty = UIImage(named:"empty")
    let heart = UIImage(named: "heart")
    var coltext: String!
    var artext: String!
    var alb: Album!
    var infav: Bool!
    var numoffav : Int!
    var indfav: IndexPath!
    var indall: IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.subv.frame = self.view.bounds
        
        self.collection.text = self.coltext
        self.artist.text = self.artext
        if self.infav{
            fav.setImage(heart, for: .normal)
        }else{
            fav.setImage(empty, for: .normal)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        self.subv.addGestureRecognizer(tapGesture)
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTap(_ sender: UIButton) {
        
        
        if self.infav{
            
            delegate?.remove(_button: self, didRemove: self.alb, atfav: self.indfav, atall: self.indall)
            fav.setImage(empty, for: .normal)
            self.infav = false
        }else{
            delegate?.add(_button: self, didAdd: self.alb,atfav: self.indfav, atall:self.indall)
            fav.setImage(heart, for: .normal)
            self.indfav = IndexPath(row: self.numoffav-1, section: 0)
            self.infav = true
        }
        
        
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // after clicking:
        // 1.add/remove from fav
        // 2.replace button image
        // 3.change icon.ishidden
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
