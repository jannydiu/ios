//
//  CollectionCollectionViewController.swift
//  album
//
//  Created by Jan on 2019/2/23.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

//private let reuseIdentifier = "Cell"

class CollectionCollectionViewController: UICollectionViewController,albumDelegate {
    func remove(_button: DetailViewController, didRemove: Album, atfav: IndexPath, atall: IndexPath) {
        
        if self.all == false{
            self.albumsFav.remove(at: atfav.row)
            self.ids.remove(at: atfav.row)
            self.collectionView.deleteItems(at: [atfav])
            self.icons[self.indsall[atfav.row].row] = true
            self.indsall.remove(at: atfav.row)
        }else{
            self.icons[atall.row] = true
            self.collectionView.reloadItems(at: [atall])
            
            let id = atfav.row
            for i in 0...self.ids.count-1{
                if id == self.ids[i]{
                    self.ids.remove(at: i)
                    self.albumsFav.remove(at: i)
                    self.indsall.remove(at: i)
                    break
                }
            }
        }
        
    }
    
    func add(_button: DetailViewController, didAdd: Album,atfav:IndexPath,atall:IndexPath) {
        self.icons[atall.row] = false
        self.ids.append(didAdd.collectionId)
        self.indsall.append(atall)
        
        
        if self.all == false{
            let indexPath = IndexPath(row: self.albumsFav.count, section: 0)
            self.albumsFav.append(didAdd)
            self.collectionView.insertItems(at:[indexPath])
        }else{
            self.albumsFav.append(didAdd)
            self.collectionView.reloadItems(at: [atall])
        }
        


    }
    
    
    

    @IBOutlet weak var segctrl: UISegmentedControl!
    
    var albumsv = AlbumService()
    var albumsAll = [Album]()
    var albumsFav = [Album]()
    var all = true
    var icons = [Bool]()
    var ids = [Int]()
    var indsall = [IndexPath]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        //self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        albumsv.search(for: "cat", completion: { albums, error in
            guard let albums = albums, error == nil else {
                print(error ?? "unknown error")
                return
            }
            print("Albums fetched: \(albums.count)")
            self.albumsAll = albums
            self.collectionView.reloadData()
            for _ in 1...self.albumsAll.count{
                self.icons.append(true)
            }
        })
        

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func segaction(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex{
        case 0:
            // show all
//            print("all")
            self.all = true
            self.collectionView.reloadData()
        case 1:
            // show favorites
//            print("fav")
            self.all = false
            self.collectionView.reloadData()
        default:
            //show all
            break
        }
    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        
        
        guard let detailViewController = segue.destination as? DetailViewController else{
            return
        }
        
        detailViewController.delegate=self
        // Pass the selected object to the new view controller.
        guard let selectedCell = sender as? albumcellviewCollectionViewCell else{
            return
        }
        guard let selectIndex = collectionView.indexPath(for: selectedCell) else{
            return
        }
        if self.all{
            let album = self.albumsAll[selectIndex.row]
            detailViewController.coltext = album.collectionName
            detailViewController.artext = album.artistName
            detailViewController.alb = album
            detailViewController.infav = !(self.icons[selectIndex.row])
            detailViewController.indall = selectIndex
            detailViewController.indfav = IndexPath(row: album.collectionId, section: 0)
            detailViewController.numoffav = self.albumsFav.count
            
        }else{
            let album = self.albumsFav[selectIndex.row]
            detailViewController.coltext = album.collectionName
            detailViewController.artext = album.artistName
            detailViewController.alb = album
            detailViewController.infav = true
            detailViewController.indfav = selectIndex
            detailViewController.indall = self.indsall[selectIndex.row]
            detailViewController.numoffav = self.albumsFav.count
        }
        
    }
    

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if self.all{
            return self.albumsAll.count
        }else{
            return self.albumsFav.count
        }
        
        
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"cell", for: indexPath) as? albumcellviewCollectionViewCell else{
                return UICollectionViewCell()
        }
        
        // downloade images from url
        //        ref : https://stackoverflow.com/questions/24231680/loading-downloading-image-from-url-on-swift
        func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
            URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
        }
        
        func downloadImage(from url: URL) {
            getData(from: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async() {
                    cell.albumimg.image = UIImage(data: data)
                }
            }
        }
        
        // Configure the cell
        if self.all{
            let album = self.albumsAll[indexPath.row]
            let url = URL(string: album.artworkUrl100 ?? "https://imgplaceholder.com/420x320/ff7f7f/333333/fa-image")
            downloadImage(from: url!)
            cell.hearticon.isHidden = self.icons[indexPath.row]
            
        }else{
            let album = self.albumsFav[indexPath.row]
            let url = URL(string: album.artworkUrl100 ?? "https://cdn.shopify.com/s/files/1/1285/7955/products/LBJ-291_1024_RGB_600x.png")
            downloadImage(from: url!)
            cell.hearticon.isHidden = false

        }
        
        return cell
    }


    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
 */

    
}
