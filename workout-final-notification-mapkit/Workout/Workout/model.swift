//
//  model.swift
//  Workout
//
//  Created by Jan on 2019/5/8.
//  Copyright © 2019 Jan. All rights reserved.
//

import Foundation

class MyFormatter {
    
    static func getDate(type: String) -> String {
        let time = Date()
        let ret = dateToString(time: time, type: type)
        return ret
    }
    
    //ref: https://stackoverflow.com/questions/27251644/how-to-get-1-hour-ago-from-a-date-in-ios-swift
    static func stringToDate(time: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm MM/dd/yy"
        var date = formatter.date(from: time)
        date?.addTimeInterval(-5*60)
        return date
    }
    
    static func dateToString(time: Date, type: String) -> String {
        let formatter = DateFormatter()
        switch type {
        case "date":
            formatter.dateFormat = "MM/dd/yyyy"
        case "time":
            formatter.dateFormat = "MM/dd/yyyy HH : mm:ss"
        default:
            formatter.dateFormat = "HH:mm MM/dd/yy"
        }
        let ret = formatter.string(from: time)
        return ret
    }
    
    static func startFormat(start: String, end: String?) -> String {
        if let end = end {
            let text = "Start: " + String(String(start.dropFirst(11)).dropLast(3)) + "\n" + "End: " + end
            return text
        } else {
            let text = "Start: " + String(String(start.dropFirst(11)).dropLast(3))
            return text
        }
    }
}

