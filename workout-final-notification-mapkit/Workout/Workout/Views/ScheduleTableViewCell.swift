//
//  ScheduleTableViewCell.swift
//  Workout
//
//  Created by Jan on 2019/6/2.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
}
