//
//  RecordTableViewCell.swift
//  Workout
//
//  Created by Jan on 2019/4/30.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class RecordTableViewCell: UITableViewCell {
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
}
