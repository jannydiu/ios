//
//  CustomSegue.swift
//  Workout
//
//  Created by Jan on 2019/6/5.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class CustomSegue: UIStoryboardSegue {
    //ref: https://github.com/uchicago-mobi/MPCS51032-2019-Spring-Forum/blob/master/Session-8/CustomSegueDemo/CustomSegueDemo/SlideUpSegue.swift
    override func perform() {
        
        let sourceView: UIView = source.view
        let destinationView: UIView = destination.view
        
        let sourceFrame = sourceView.frame
        
        destinationView.frame = CGRect(x: 0.0, y: sourceFrame.height, width: sourceFrame.width, height: sourceFrame.height)
        
        let window = UIApplication.shared.keyWindow
        window?.insertSubview(destinationView, aboveSubview: sourceView)
        
        let animator = UIViewPropertyAnimator(duration: 1.0, curve: .easeInOut) {
            sourceView.frame = sourceView.frame.offsetBy(dx: 0.0, dy: -sourceFrame.height)
            
            destinationView.frame = destinationView.frame.offsetBy(dx: 0.0, dy: -sourceFrame.height)
        }
        
        animator.addCompletion { _ in
            self.source.present(self.destination, animated: false, completion: nil)
        }
        
        animator.startAnimation()
    }

}
