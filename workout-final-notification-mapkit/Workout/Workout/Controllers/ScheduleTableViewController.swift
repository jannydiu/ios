//
//  ScheduleTableViewController.swift
//  Workout
//
//  Created by Jan on 2019/6/2.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit
import UserNotifications

class ScheduleTableViewController: UITableViewController, UNUserNotificationCenterDelegate {
    
    let center = UNUserNotificationCenter.current()
    var categories: [String] = UserDefaults(suiteName: "group.WorkoutSiri")!.stringArray(forKey: "categories") ?? []
    var schedule: [String: String] = UserDefaults(suiteName: "group.WorkoutSiri")?.value(forKey: "schedule") as? [String : String] ?? [String:String]()
    var times: [String] {
        return schedule.map{$0.key}
    }
    var workouts: [String] {
        return schedule.map{$0.value}
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        center.delegate = self
        //ref: https://learnappmaking.com/local-notifications-scheduling-swift/
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addAction))
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        categories = UserDefaults(suiteName: "group.WorkoutSiri")!.stringArray(forKey: "categories") ?? []
    }
    
    private func checkAuthSetNotif(time: Date) {
        center.getNotificationSettings { (setting) in
            switch setting.authorizationStatus {
            case .notDetermined:
                self.center.requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
                    if granted != true || error != nil {
                        print("don't send notification")
                    }
                }
            case .authorized, .provisional:
                self.scheduleNotification(time: time)
            default:
                break
            }
        }
    }
    
    func scheduleNotification(time: Date) {
        let content = UNMutableNotificationContent()
        content.title = "Time for workout"
        content.body = "A scheduled workout starts in 5 minutes"
        //ref https://roadfiresoftware.com/2015/08/how-to-get-components-from-a-date-in-swift/
        let tcp = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute], from: time)
        //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let trigger = UNCalendarNotificationTrigger(dateMatching: tcp, repeats: false)
        let request = UNNotificationRequest(identifier: "notice", content: content, trigger: trigger)
        center.add(request) { (_) in
        }
    }
    
    //deal with notification when the app is in background
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        var d = Date()
        d.addTimeInterval(5*60)
        let dts = MyFormatter.dateToString(time: d, type: "default")
        removeSchedule(time: dts)
    }
    
    //deal with notification when the app is in use
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let remindAlert = UIAlertController(title: "About to start a workout", message: "A scheduled workout starts in 5 minutes", preferredStyle: .alert)
        var d = Date()
        d.addTimeInterval(5*60)
        let dts = MyFormatter.dateToString(time: d, type: "default")
        remindAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.removeSchedule(time: dts)
        }))
        self.present(remindAlert, animated: true)
    }
    
    func removeSchedule(time: String) {
        self.schedule.removeValue(forKey: time)
        UserDefaults(suiteName: "group.WorkoutSiri")?.setValue(self.schedule, forKey: "schedule")
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return schedule.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "schedulecell", for: indexPath) as? ScheduleTableViewCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        let time = times[indexPath.row]
        let workout = workouts[indexPath.row]
        let start = MyFormatter.stringToDate(time: time)
        if start! < Date() {
            removeSchedule(time: time)
            return UITableViewCell()
        }
        cell.nameLabel.text = workout
        cell.timeLabel.text = time
        return cell
    }
    
    //ref https://stackoverflow.com/questions/26567413/get-input-value-from-textfield-in-ios-alert-in-swift
    @objc func addAction() {
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Add new schedule", message: "Create a new schedule for one workout category already added in home page", preferredStyle: .alert)
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Required: Workout"
            textField.adjustsFontSizeToFitWidth = true
        }
        alert.addTextField { (textField) in
            textField.placeholder = "Required: 19:00 06/02/19"
        }
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let value = alert?.textFields![0].text
            let key = alert?.textFields![1].text
            if key == "" || value == "" {
                self.inputCheck()
                return
            }
            let start = MyFormatter.stringToDate(time: key!)
            let now = Date()
            if start == nil || start! < now || !self.categories.contains(value!) {
                self.inputCheck()
                return
            } else {
                self.schedule[key!] = value
                UserDefaults(suiteName: "group.WorkoutSiri")?.setValue(self.schedule, forKey: "schedule")
                self.tableView.reloadData()
                self.checkAuthSetNotif(time: start!)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancle", style: .default, handler: nil))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    func inputCheck() {
        let inputAlert = UIAlertController(title: "Input Error", message: "Please check your input. 1)You have to provide both fields in formate 2) Workout name has to exit in your home category page before schedule 3) Start time should be in the at least 5min-future and could not be the same with any existing schedule", preferredStyle: .alert)
        inputAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(inputAlert, animated: true)
    }
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            
            let time = times[indexPath.row]
            removeSchedule(time: time)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

}
