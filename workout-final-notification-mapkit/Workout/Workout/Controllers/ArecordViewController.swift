//
//  ArecordViewController.swift
//  Workout
//
//  Created by Jan on 2019/4/30.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation

class ArecordViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var startLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var endButton: UIButton!
    
    
    var coords: [CLLocationCoordinate2D] = []
    var distance = UserDefaults(suiteName: "group.WorkoutSiri")!.double(forKey: "inProgressRun")
    var locationManager: CLLocationManager = CLLocationManager()
    var distanceLabel: UILabel?
    var map: MKMapView?
    
    var end: String {
        let time = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH : mm"
        return formatter.string(from: time)
    }
    var date: String!
    var start: String!
    var status: String!
    var startRun: Bool = false
    var startYoga: Bool = false
    var audioPlayer = AVAudioPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if startRun {
            mapSetUp()
        } else if startYoga {
            yogaSetUp()
        } else {
            otherSetUp()
        }
        self.navigationItem.title = date
        if (start.contains("End")) {
            startLabel.text = start
        } else {
            let substart = "Start: " + String(String(start.dropLast(3)).dropFirst(11))
            startLabel.text = substart
        }
        statusLabel.text = status
        if (status == Optional("Workout completed ")) {
            endButton.isEnabled = false
            endButton.isHidden = true
        }
    }
    
    func otherSetUp() {
        let imgv = UIImageView()
        imgv.frame = containerView.bounds
        imgv.image = UIImage(named: "slogan")
        self.containerView.addSubview(imgv)
    }
    
    func yogaSetUp() {
        let imgv = UIImageView()
        imgv.frame = containerView.bounds
        imgv.image = UIImage(named: "workout")
        imgv.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapAction))
        imgv.addGestureRecognizer(tap)
        self.containerView.addSubview(imgv)
    }
    
    @objc func tapAction(tap: UITapGestureRecognizer) {
        let path = Bundle.main.path(forResource: "yoga", ofType: "mp3")!
        let url = URL(fileURLWithPath: path)
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer.play()
        } catch {
            print("couldn't load the file")
        }
    }
    
    func mapSetUp() {
        //for location
        map = MKMapView()
        map?.frame = containerView.bounds
        containerView.addSubview(map!)
        map!.delegate = self
        map!.mapType = .standard
        map!.showsUserLocation = true
        map!.userTrackingMode = .follow
        map!.isScrollEnabled = true
        map!.isZoomEnabled = true
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            coords.append(locationManager.location?.coordinate ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
            //for label
            distanceLabel = UILabel(frame: CGRect(x: self.view.frame.midX-100, y: 60, width: 200, height: 50))
            self.distanceLabel?.text = "Distance: " + String(format: "%.2f", distance)
            self.view.addSubview(distanceLabel!)
        }
    }
    
    //ref:https://robkerr.com/adding-a-mkpolyline-overlay-using-swift-to-an-ios-mapkit-map/
    func addPolyline(coords: [CLLocationCoordinate2D]) {
        let polyline = MKPolyline(coordinates: coords, count: coords.count)
        map?.addOverlay(polyline)
    }
    
    //ref:https://robkerr.com/adding-a-mkpolyline-overlay-using-swift-to-an-ios-mapkit-map/
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let pr = MKPolylineRenderer(overlay: overlay)
        pr.strokeColor = UIColor.red.withAlphaComponent(0.5)
        pr.lineWidth = 5
        return pr
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        let loc1 = CLLocation(latitude: coords[coords.count-1].latitude, longitude: coords[coords.count-1].longitude)
        let loc2 = userLocation.location
        let dist = loc2!.distance(from: loc1)
        self.distance += dist/1609.344
        UserDefaults(suiteName: "group.WorkoutSiri")!.set(distance, forKey: "inProgressRun")
        self.distanceLabel?.text = "Distance: " + String(format: "%.2f", distance)
        self.coords.append(userLocation.coordinate)
        self.addPolyline(coords: self.coords)
    }
}
