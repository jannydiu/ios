//
//  WorkoutUnitTests.swift
//  WorkoutUnitTests
//
//  Created by Jan on 2019/6/5.
//  Copyright © 2019 Jan. All rights reserved.
//

import XCTest

class WorkoutUnitTests: XCTestCase {

    func testdateToString() {
        let d = Date()
        let retd = MyFormatter.dateToString(time: d, type: "date")
        XCTAssertEqual(retd, "06/05/2019")
        let rett = MyFormatter.dateToString(time: d, type: "time").dropLast(8)
        XCTAssertEqual(rett, "06/05/2019 11")
        let retdf = MyFormatter.dateToString(time: d, type: "default")
        XCTAssertEqual(retdf, "11:01 06/05/19")
    }
    
    func teststingToDate() {
        let ret = MyFormatter.stringToDate(time: "11:00 06/05/19")
        let d = Date().addingTimeInterval(-5*60)
        XCTAssertEqual(ret, d)
    }
    
    func testStartFormatter() {
        let start = MyFormatter.startFormat(start: "05/09/2019 19 : 20:22", end: nil)
        XCTAssertEqual(start, "Start: 19 : 20")
        let end = MyFormatter.startFormat(start: "05/09/2019 19 : 20:22", end: "19 : 50")
        XCTAssertEqual(end, "Start: 19 : 20\nEnd: 19 : 50")
    }

}
