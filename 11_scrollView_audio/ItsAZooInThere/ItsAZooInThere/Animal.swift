//
//  Animal.swift
//  ItsAZooInThere
//
//  Created by Jan on 2019/6/26.
//  Copyright © 2019 techbar. All rights reserved.
//

import UIKit

class Animal: CustomStringConvertible{
    let name: String
    let species: String
    let age: Int
    var image: UIImage
    let soundPath: String
    var description: String{
        return "Animal: name = \(name), species = \(species), age = \(age)"
    }
    
    init(name:String,species:String,age:Int,image:String,soundPath:String) {
        self.name = name
        self.species = species
        self.age = age
        self.image = UIImage(named: image)!
        self.soundPath = soundPath
    }
    
}

