//
//  ViewController.swift
//  ItsAZooInThere
//
//  Created by techbar on 1/16/19.
//  Copyright © 2019 techbar. All rights reserved.
//

import UIKit
import AVFoundation


class ViewController: UIViewController {
    
    var animals = [Animal]()
    var audioPlayer: AVAudioPlayer = AVAudioPlayer()

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        scrollView.contentSize = CGSize(width:1125,height:500)
        myAnimals()
        animals = animals.shuffled()
        label.text = animals[0].species
//        add buttons and images
        for index in 0..<animals.count{
            let button = UIButton(type:.system)
            button.setTitle(animals[index].name, for: .normal)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
            button.tag = index
            button.addTarget(self, action: #selector(buttonTapped(sender:)), for: .touchUpInside)
            let location1 = CGRect(x:375*index+375/2-50, y: 10, width: 100, height: 100)
            addElementToScrollView(button, at: location1)
            let imageView = UIImageView()
            imageView.image = animals[index].image
            let location2 = CGRect(x:375*index, y: 120, width: 375, height: 375)
            addElementToScrollView(imageView, at: location2)
        }
    }
    
    func myAnimals() {
        let animal1 = Animal(name: "Cookie", species: "Shiba Inu", age: 1,image:"d",soundPath:"Sounds/dog")
        let animal2 = Animal(name: "Banana", species: "Elephant", age: 2,image:"e",soundPath:"Sounds/elephant")
        let animal3 = Animal(name: "Meme", species: "Fox", age: 3,image:"f",soundPath:"Sounds/fox")
        animals.append(animal1)
        animals.append(animal2)
        animals.append(animal3)
    }
    
    func addElementToScrollView(_ element: UIView, at postion: CGRect) {
        element.frame = postion
        scrollView.addSubview(element)
    }
    
    @objc func buttonTapped(sender: UIButton!) {
        let btnsendtag: UIButton = sender
        let animal = animals[btnsendtag.tag]

//        sound
//        https://stackoverflow.com/questions/46231153/how-to-add-sound-files-to-your-bundle-in-xcode
        
        let path = Bundle.main.path(forResource: animal.soundPath, ofType: "mp3")!
        let url = URL(fileURLWithPath: path)
        
        do {
            //create your audioPlayer in your parent class as a property
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer.play()
        } catch {
            print("couldn't load the file")
        }
        
//        alert
        let myalert = UIAlertController(title: animal.name, message: "This \(animal.species) is \(animal.age) years old.", preferredStyle:.alert)
        let action = UIAlertAction(title: "OK", style: .default) { (_) in
            print(animal)
            self.audioPlayer.stop()
        }
        myalert.addAction(action)
        self.present(myalert, animated: true)
    }
}


extension ViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView:UIScrollView){
//        https://stackoverflow.com/questions/18665307/how-to-change-image-background-when-uiscrollview-being-scrolled
        let test = scrollView.contentOffset.x / scrollView.frame.size.width
        if test<0 {
            label.text = animals[0].species
            UIView.animate(withDuration: 0.1, animations: {
                self.label.alpha = 1+test //0 to fade out. 1 to fade in
            })
        }else if test<=0.5{
            label.text = animals[0].species
            UIView.animate(withDuration: 0.1, animations: {
                self.label.alpha = 1-2*test //0 to fade out. 1 to fade in
            })
        }else if test<=1{
            label.text = animals[1].species
            UIView.animate(withDuration: 0.1, animations: {
                self.label.alpha = 2*test-1 //0 to fade out. 1 to fade in
            })
        }else if test<=1.5{
            label.text = animals[1].species
            UIView.animate(withDuration: 0.1, animations: {self.label.alpha = -2*test + 3})
        }else if test<=2{
            label.text = animals[2].species
            UIView.animate(withDuration: 0.1, animations: {
                self.label.alpha = 2*test-3 //0 to fade out. 1 to fade in
            })
        }else{
            label.text = animals[2].species
            UIView.animate(withDuration: 0.1, animations: {
                self.label.alpha = -2*test+5 //0 to fade out. 1 to fade in
            })
        }
    }
}

