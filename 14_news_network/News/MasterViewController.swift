//
//  MasterViewController.swift
//  News
//
//  Created by Jan on 2019/2/16.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController,UISearchBarDelegate,UISplitViewControllerDelegate {

    
    var searchBar = UISearchBar()
    var searchTerm = "Apple"
    let newsService = NewsService()
    var someNews: [News]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView = searchBar
        searchBar.placeholder = "search"
        splitViewController?.preferredDisplayMode = .allVisible
        //splitViewController?.delegate = self
        searchBar.delegate = self
        
        print(self.searchTerm)
        newsService.search(for: self.searchTerm, completion: { somenews, error in
            guard let someNews = somenews, error == nil else{
                print(error ?? "unknown error")
                // alart
                let alert = UIAlertController(title: "Error", message: "Please check your network :(", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    print("alert shown up")
                }))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            self.someNews = someNews
            self.tableView.reloadData()
        })
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    
    //ref: https://developer.apple.com/documentation/uikit/uisearchbar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //ref: https://stackoverflow.com/questions/35480530/swift-remove-white-spaces-from-string-doesnt-work
        let searchtext = searchBar.text!
        let puretext = searchtext.components(separatedBy: .whitespaces)
        self.searchTerm = puretext.joined()
        
        print(self.searchTerm)
        
        newsService.search(for: self.searchTerm) { (somenews, error) in
            guard let someNews = somenews, error == nil else{
                print(error ?? "unknown error")
                //alart
                let alert = UIAlertController(title: "Error", message: "Please check your network :(", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    print("alert shown up")
                }))
                self.present(alert, animated: true, completion: nil)

                return
            }
            self.someNews = someNews
            self.tableView.reloadData()
    
            if self.someNews.count == 0{
                let alert = UIAlertController(title: "Error", message: "Please try another search term", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    print("alert shown up")
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.someNews == nil {
            return 0
        } else{
            return someNews.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "newscellid", for: indexPath) as? NewsCell else {return UITableViewCell()}

        // Configure the cell...
        let news = someNews[indexPath.row]
        cell.titleLabel.text = news.title
        cell.authorLabel.text = news.author
        
            // RecentLabel
        // ref: https://stackoverflow.com/questions/33605816/first-and-last-day-of-the-current-month-in-swift
        let calendar = NSCalendar.current
//        let components = calendar.component(.month, from: NSDate() as Date)
        let components = calendar.dateComponents([.year,.month], from: NSDate() as Date)
        let month = components.month
        let year = components.year!
        var start = news.publishedAt!.index(news.publishedAt!.startIndex, offsetBy: 5)
        var end = news.publishedAt!.index(news.publishedAt!.startIndex, offsetBy: 7)
        let nmonth = Int(news.publishedAt![start..<end])
        start = news.publishedAt!.startIndex
        end = news.publishedAt!.index(news.publishedAt!.startIndex, offsetBy: 4)
        let nyear = Int(news.publishedAt![start..<end])
//        let publishdate = nyear+nmonth
//        let currdate = year+month
        if (month==nmonth)&&(year==nyear){
            cell.recentLabel.text = "Recent"
            cell.recentLabel.isHidden = false
        }else{
            cell.recentLabel.isHidden = true
        }

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        guard let detailViewController = segue.destination as? DetailViewController else { return }
        // Pass the selected object to the new view controller.
        guard let selectedIndexPath = tableView.indexPathForSelectedRow else { return }
        let news = self.someNews![selectedIndexPath.row]
        print(news.url ?? "default")
        detailViewController.newsURL = news.url!
        detailViewController.sourcename = news.source.name
        
    }
    
//    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
//        return true
//    }
    

}
