//
//  news.swift
//  News
//
//  Created by Jan on 2019/2/16.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

struct NewsResult: Codable {
    let status: String
    let totalResults: Int
    let articles: [News]
}

struct News: Codable{
    let source: Source!
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
}

struct Source: Codable {
    let id: String?
    let name: String!
}

class NewsService {
    var dataTask: URLSessionDataTask?
    
    private let urlString = "https://newsapi.org/v2/everything?q="
    
    func search(for searchTerm:String, completion: @escaping ([News]?, Error?) -> ()) {
        guard let url = URL(string: urlString + searchTerm) else {
            print("invalid url: \(urlString + searchTerm)")
            return
        }
        print(url)
        var urlRequest = URLRequest(url: url)
        urlRequest.addValue("6978ef9c02bc44d0b7b55e042154c64e", forHTTPHeaderField: "X-Api-Key")
        
        
        let task = URLSession.shared.dataTask(with:urlRequest) { data, response, error in
            
            guard let data = data, let response = response as? HTTPURLResponse, error == nil else {
                DispatchQueue.main.async { completion(nil, error) }
                return
            }
            
            print("Status code: \(response.statusCode)")
//            print(String(data: data, encoding: .utf8) ?? "unable to print data")
            
            do {
                let decoder = JSONDecoder()
                let newsResult = try decoder.decode(NewsResult.self, from: data)
                DispatchQueue.main.async { completion(newsResult.articles, nil) }
            } catch (let error) {
                
                DispatchQueue.main.async { completion(nil, error) }
                
            }
        }
        task.resume()
        print("news done")
    }
}
