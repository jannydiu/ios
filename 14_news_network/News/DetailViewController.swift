//
//  DetailViewController.swift
//  News
//
//  Created by Jan on 2019/2/16.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController, WKNavigationDelegate,WKUIDelegate {
    
    
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var sourceLable: UILabel!
    
    var newsURL: String = "https://google.com"
    var url: URL!
    var activityind  = UIActivityIndicatorView()
    
    var sourcename = "source"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("d1",newsURL)
        self.url = URL(string: newsURL)
        let request = URLRequest(url: self.url)
        webView.navigationDelegate = self
        webView.load(request)
        print(self.sourcename)
        print("d2")

        // Do any additional setup after loading the view.
    }
    

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("start")
        
        
        self.sourceLable.text = self.sourcename
        // ref https://www.youtube.com/watch?v=dLfOdObZW7k
        self.activityind.center = webView.center
        self.activityind.style = UIActivityIndicatorView.Style.gray
        self.activityind.hidesWhenStopped = true
        self.view.addSubview(activityind)
        self.view.bringSubviewToFront(activityind)
        self.activityind.startAnimating()

    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("ee")

        // error label
        webView.isHidden = true
        let errorLable = UILabel()
        errorLable.text = "Oops, something went wrong :("
        errorLable.frame.size.width = 300
        errorLable.frame.size.height = 100
        errorLable.center = self.webView.center
        errorLable.textColor = UIColor.blue
        errorLable.font = UIFont(name:"System", size: 50)
        self.view.addSubview(errorLable)
        self.view.bringSubviewToFront(errorLable)
        self.sourceLable.isHidden = true
        


        print(error.localizedDescription)
        self.activityind.stopAnimating()
        webView.stopLoading()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("end")
        self.activityind.stopAnimating()
        webView.stopLoading()

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

