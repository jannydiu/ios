//
//  GameScene.swift
//  HitAndScore
//
//  Created by Vamshi Krishna on 26/01/18.
//  Copyright © 2018 Vamshi Krishna. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var coinMan : SKSpriteNode!
    var coinTimer : Timer?
    var bombTimer:Timer?
    var ceil : SKSpriteNode!
    var brickTop: SKSpriteNode!
    var background: SKSpriteNode = SKSpriteNode(imageNamed: "bkgd")
    var yourScoreLabel : SKLabelNode?
    var finalScoreLabel : SKLabelNode?
    var scoreLabel : SKLabelNode?
    var timeLabel: SKLabelNode!
    
    let coinManCategory : UInt32 = 0x1 << 1
    let coinCategory : UInt32 = 0x1 << 2
    let bombCategory : UInt32 = 0x1 << 3
    let groundAndCeilCategory : UInt32 = 0x1 << 4
    
    var score = 0
    var seconds = 0
    var duration = 60
    var groundHeight: CGFloat!
    var coinManPos: CGPoint!
    var groundInd: Int = 0
    var isJumping: Bool = false
    
    override func didMove(to view: SKView) {
        
        
        //ref:https://stackoverflow.com/questions/31307213/background-image-for-sprite-kit-scene
        background.position = CGPoint(x: 0, y: 0)
        background.size = size
        background.zPosition = -0.1
        addChild(background)
 
        
        physicsWorld.contactDelegate = self
        
        coinMan = childNode(withName: "coinMan") as? SKSpriteNode
        coinMan?.physicsBody?.categoryBitMask = coinManCategory
        //coinMan?.physicsBody?.contactTestBitMask = coinCategory | bombCategory
        coinMan?.physicsBody?.collisionBitMask = groundAndCeilCategory
        coinMan?.physicsBody?.contactTestBitMask = (coinMan?.physicsBody?.collisionBitMask)!
        coinManPos = coinMan.position
        
        marioMove(option: "run")
        
        ceil = childNode(withName: "ceil") as? SKSpriteNode
        ceil?.physicsBody?.categoryBitMask = groundAndCeilCategory
        ceil?.physicsBody?.collisionBitMask = coinManCategory
        
        scoreLabel = childNode(withName: "scoreLabel") as? SKLabelNode
        timeLabel = childNode(withName: "timeLabel") as? SKLabelNode
       startTimers()
        createGrass()
    }
    
    func marioMove(option: String) {
        if option == "run" {
            var coinManRun : [SKTexture] = []
            coinManRun.append(SKTexture(imageNamed: "Mario-\(1)"))
            coinManRun.append(SKTexture(imageNamed: "Mario-\(2)"))
            coinManRun.append(SKTexture(imageNamed: "Mario-\(3)"))
            coinManRun.append(SKTexture(imageNamed: "Mario-\(2)"))
            coinMan?.run(SKAction.repeatForever(SKAction.animate(with: coinManRun, timePerFrame: 0.2)))
        } else {
            coinMan?.texture = SKTexture(imageNamed: "Mario-3")
        }
        
    }
    
    func createGrass() {
        let sizingGrass = SKSpriteNode(imageNamed: "Ground")
        let numberOfGrass = Int(size.width / sizingGrass.size.width)
        for number in 0...numberOfGrass {
            let grass = SKSpriteNode(imageNamed: "Ground")
            grass.physicsBody = SKPhysicsBody(rectangleOf: grass.size)
            grass.physicsBody?.categoryBitMask = groundAndCeilCategory
            grass.physicsBody?.collisionBitMask = coinManCategory
            grass.physicsBody?.affectedByGravity = false
            grass.physicsBody?.isDynamic = false
            addChild(grass)
            
            self.groundHeight = grass.size.height
            
            let grassX = -size.width / 2 + grass.size.width / 2 + grass.size.width * CGFloat(number)
            grass.position = CGPoint(x: grassX, y: -size.height / 2 + grass.size.height / 2)
            let speed = 150.0
            let firstMoveLeft = SKAction.moveBy(x: -grass.size.width - grass.size.width * CGFloat(number), y: 0, duration: TimeInterval(grass.size.width + grass.size.width * CGFloat(number)) / speed)
            
            var moveDistance = CGFloat(0)
            if self.groundInd == 0 {
                self.groundInd += 1
                moveDistance = CGFloat(100)
            } else {
                self.groundInd = 0
                moveDistance = CGFloat(0)
            }

            let resetGrass = SKAction.moveBy(x: size.width + grass.size.width, y: moveDistance, duration: 0)
            let grassFullMove = SKAction.moveBy(x: -size.width - grass.size.width, y: 0, duration: TimeInterval(size.width + grass.size.width) / speed)
            let grassMovingForver = SKAction.repeatForever(SKAction.sequence([grassFullMove,resetGrass]))
            
            grass.run(SKAction.sequence([firstMoveLeft,resetGrass,grassMovingForver]))
        }
    }
    
    func startTimers() {
        coinTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.createCoin()
        })
        
        bombTimer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: { (timer) in
            let num = arc4random_uniform(3)
            self.createBomb(numbers: Int(num))
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if scene?.isPaused == false {
            coinMan?.physicsBody?.applyForce(CGVector(dx: 0, dy: 100_000))
            coinMan.removeAllActions()
            coinMan.texture = SKTexture(imageNamed: "Mario-3")
            self.isJumping = true
        }
        
        let touch = touches.first
        if let location = touch?.location(in: self) {
            let theNodes = nodes(at: location)
            
            for node in theNodes {
                if node.name == "play" {
                    score = 0
                    self.seconds = 0
                    coinMan.position = coinManPos
                    coinMan.position.x += 15.0
                    node.removeFromParent()
                    finalScoreLabel?.removeFromParent()
                    yourScoreLabel?.removeFromParent()
                    scene?.isPaused = false
                    scoreLabel?.text = "Score: \(score)"
                    startTimers()
 
                }
            }
        }
    }
    
    func createCoin() {
        timeLabel.text = "Time: \(self.duration-self.seconds)"
        if self.seconds == self.duration {
            gameOver(result: "succcess")
            return
        }
        self.seconds += 1
        let coin = SKSpriteNode(imageNamed: "coin")
        coin.size = CGSize(width: 50.0, height: 50.0)
        coin.physicsBody = SKPhysicsBody(rectangleOf: coin.size)
        coin.physicsBody?.affectedByGravity = false
        coin.physicsBody?.categoryBitMask = coinCategory
        coin.physicsBody?.contactTestBitMask = coinManCategory
        coin.physicsBody?.collisionBitMask = 0
        addChild(coin)
        
        
        let maxY = size.height / 2 - coin.size.height / 2
        let minY = -size.height / 2 + coin.size.height / 2 + self.groundHeight
        let range = maxY - minY
        let coinY = maxY - CGFloat(arc4random_uniform(UInt32(range)))
        
        coin.position = CGPoint(x: size.width / 2 + coin.size.width / 2, y: coinY)
        
        let brick = SKSpriteNode(imageNamed: "brick")
        brick.size = CGSize(width: 50.0, height: 50.0)
        brick.physicsBody = SKPhysicsBody(rectangleOf: brick.size)
        brick.physicsBody?.categoryBitMask = groundAndCeilCategory
        brick.physicsBody?.collisionBitMask = coinManCategory
        brick.physicsBody?.affectedByGravity = false
        brick.physicsBody?.isDynamic = false
        //addChild(top)
        //top.position = CGPoint(x: coin.position.x, y: coinY+coin.size.height/2)
        coin.addChild(brick)
        
        
        
        let moveLeft = SKAction.moveBy(x: -size.width - coin.size.width, y: 0, duration: 4)
        
        coin.run(SKAction.sequence([moveLeft, SKAction.removeFromParent()]))
        brick.run(SKAction.sequence([moveLeft, SKAction.removeFromParent()]))
    }
    
    func createBomb(numbers: Int) {
        
        for i in 0...numbers {
            let bomb = SKSpriteNode(imageNamed: "bomb")
            bomb.size = CGSize(width: 50.0, height: 50.0)
            bomb.physicsBody = SKPhysicsBody(rectangleOf: bomb.size)
            bomb.physicsBody?.affectedByGravity = true
            bomb.physicsBody?.categoryBitMask = bombCategory
            bomb.physicsBody?.contactTestBitMask = coinManCategory
            bomb.physicsBody?.collisionBitMask = groundAndCeilCategory
            addChild(bomb)
            
            let bombY = -size.height / 2 + bomb.size.height / 2 + self.groundHeight
            bomb.position = CGPoint(x: size.width / 3 + bomb.size.width / 2 + CGFloat(i)*bomb.size.width, y: bombY)
            let moveLeft = SKAction.moveBy(x: -size.width - bomb.size.width, y: 0, duration: 2)
            
            bomb.run(SKAction.sequence([moveLeft, SKAction.removeFromParent()]))
        }
        

        /*
        let maxY = size.height / 2 - bomb.size.height / 2
        let minY = -size.height / 2 + bomb.size.height / 2
        let range = maxY - minY
        let bombY = maxY - CGFloat(arc4random_uniform(UInt32(range)))
        
        bomb.position = CGPoint(x: size.width / 2 + bomb.size.width / 2, y: bombY)
        
        let moveLeft = SKAction.moveBy(x: -size.width - bomb.size.width, y: 0, duration: 4)
        
        bomb.run(SKAction.sequence([moveLeft, SKAction.removeFromParent()]))
 */
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        if contact.bodyA.categoryBitMask == coinCategory {
            contact.bodyA.node?.removeFromParent()
            score += 1
            scoreLabel?.text = "Score: \(score)"
        }
        if contact.bodyB.categoryBitMask == coinCategory {
            contact.bodyB.node?.removeFromParent()
            score += 1
            scoreLabel?.text = "Score: \(score)"
        }
        
        if contact.bodyA.categoryBitMask == bombCategory {
            contact.bodyA.node?.removeFromParent()
            gameOver(result: "fail")
        }
        
        if contact.bodyB.categoryBitMask == bombCategory {
            contact.bodyB.node?.removeFromParent()
            gameOver(result: "fail")
        }
        
        
        if contact.bodyA.categoryBitMask == groundAndCeilCategory {
            self.isJumping = false
            marioMove(option: "run")
        }
        
        if contact.bodyB.categoryBitMask == groundAndCeilCategory {
            self.isJumping = false
            marioMove(option: "run")
        }
 
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        //ref:https://stackoverflow.com/questions/30551951/how-do-you-tell-if-a-node-is-on-the-screen-spritekit-swift
        if (!intersects(coinMan)){
            gameOver(result: "fail")
        }
    }
    
    func gameOver(result: String) {
        
        scene?.isPaused = true
        
        coinTimer?.invalidate()
        bombTimer?.invalidate()
        
        if result == "fail" {
            yourScoreLabel = SKLabelNode(text: "Game over")
            finalScoreLabel = SKLabelNode(text: "😔")
        } else {
            yourScoreLabel = SKLabelNode(text: "Win! Your Score:")
            finalScoreLabel = SKLabelNode(text: "\(score)")
        }
        //yourScoreLabel = SKLabelNode(text: "Your Score:")
        yourScoreLabel?.position = CGPoint(x: 0, y: 200)
        yourScoreLabel?.fontSize = 100
        //yourScoreLabel?.fontColor = UIColor.blue
        yourScoreLabel?.zPosition = 1
        if yourScoreLabel != nil {
            addChild(yourScoreLabel!)
        }
        
        //finalScoreLabel = SKLabelNode(text: "\(score)")
        finalScoreLabel?.position = CGPoint(x: 0, y: 0)
        finalScoreLabel?.fontSize = 200
        //finalScoreLabel?.fontColor = UIColor.blue
        finalScoreLabel?.zPosition = 1
        if finalScoreLabel != nil {
            addChild(finalScoreLabel!)
        }
        
        let playButton = SKSpriteNode(imageNamed: "play")
        playButton.position = CGPoint(x: 0, y: -200)
        playButton.name = "play"
        playButton.zPosition = 1
        addChild(playButton)
    }
    
}
