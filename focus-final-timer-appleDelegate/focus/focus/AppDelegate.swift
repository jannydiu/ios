//
//  AppDelegate.swift
//  focus
//
//  Created by Jan on 2019/3/6.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // splash screen
        self.splashScreen()
        
        // for rating
        StoreKitHelper.displayStoreKit()
        StoreKitHelper.incrementNumberOfTimeLaunched()
        
        // for setting first-launch-date
        if StoreKitHelper.first {
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let strDate = formatter.string(from: date as Date)
        
        UserDefaults.standard.set(strDate, forKey: "date")

        
        
            
        }
    
        
        return true
    }
    
    func splashScreen() {
        let launchVC = UIStoryboard.init(name: "LaunchScreen",bundle: nil)
        let rootVC = launchVC.instantiateViewController(withIdentifier: "splashController")
        self.window?.rootViewController = rootVC
        self.window?.makeKeyAndVisible()
        
        // add image to splash screen
        let imgView = UIImageView()
        let img = UIImage(named: "sp2")
        imgView.image = img
        imgView.frame = rootVC.view.frame
        rootVC.view.addSubview(imgView)
        
        // display version
        let versionLabel = UILabel()
        let version: String = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        versionLabel.text = "version: \(version)"
        versionLabel.font = .systemFont(ofSize: 20)
        versionLabel.textColor = UIColor.white
        versionLabel.frame = CGRect(x: rootVC.view.center.x-50, y: rootVC.view.center.y-100, width: 300, height: 200)
        rootVC.view.addSubview(versionLabel)
        rootVC.view.bringSubviewToFront(versionLabel)

        // display splash screen for 2 seconds
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.dismissSplash), userInfo: nil, repeats: false)
    }
    
    @objc func dismissSplash() {
        let mainVC = UIStoryboard.init(name: "Main", bundle: nil)
        let rootVC = mainVC.instantiateViewController(withIdentifier: "initController")
        self.window?.rootViewController = rootVC
        self.window?.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

