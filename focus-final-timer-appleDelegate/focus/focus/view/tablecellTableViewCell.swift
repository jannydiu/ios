//
//  tablecellTableViewCell.swift
//  focus
//
//  Created by Jan on 2019/3/19.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class tablecellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var record: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        record.textColor = UIColor.white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
