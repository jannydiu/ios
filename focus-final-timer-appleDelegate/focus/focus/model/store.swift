//
//  store.swift
//  focus
//
//  Created by Jan on 2019/3/13.
//  Copyright © 2019 Jan. All rights reserved.
//

import Foundation
import StoreKit


struct StoreKitHelper {
    static let numberOfTimesLaunchedKey = "numberOfTimesLaunched"
    
    static var first = false
    
    static func displayStoreKit() {
//        // version check
//        guard let currentVersion = Bundle.main.object(forInfoDictionaryKey: kCFBundleNameKey as String) as? String else {
//            return
//        }
//        let lastVersionPromptedForReview = UserDefaults.standard.string(forKey: "lastVersion")
        
        let numberOfTimesLaunched: Int = UserDefaults.standard.integer(forKey: StoreKitHelper.numberOfTimesLaunchedKey)
        
        if numberOfTimesLaunched == 0 {
            self.first = true
        } else {
            self.first = false
        }
        
        
        
        if numberOfTimesLaunched == 2 {
            SKStoreReviewController.requestReview()
        }
    }
    
    static func incrementNumberOfTimeLaunched() {
        let numberOfTimesLaunched: Int = UserDefaults.standard.integer(forKey: StoreKitHelper.numberOfTimesLaunchedKey) + 1
        UserDefaults.standard.set(numberOfTimesLaunched, forKey: StoreKitHelper.numberOfTimesLaunchedKey)
    }
}
