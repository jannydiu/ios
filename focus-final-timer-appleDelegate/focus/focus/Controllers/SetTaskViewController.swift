//
//  ViewController.swift
//  focus
//
//  Created by Jan on 2019/3/6.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit


class SetTaskViewController: UIViewController {

    
    
    @IBOutlet weak var timerlabel: UILabel!
    @IBOutlet weak var startbutton: UIButton!
    
    @IBOutlet weak var starsview: UIView!
    @IBOutlet weak var timeview: UIView!
    @IBOutlet weak var datepicker: UIDatePicker!
    var timer: Timer!
    var checkstatetimer: Timer!
    var tasktime: TimeInterval!
    let blackscreen = UIButton()
    var recordtime: Int!
    var randompos: CGRect!
    var radius: CGFloat!
    var imgind: Int!
    var records: [String]! = nil
    var recordperiod: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = UIColor.black
        datepicker.backgroundColor = UIColor.black
        datepicker.setValue(UIColor.white, forKeyPath: "textColor")

        
        recordtime = UserDefaults.standard.integer(forKey: "record")
        starsview.backgroundColor = UIColor.black
        
        for _ in 0...recordtime{
            self.randomPosition()
            let img = UIImage(named: "\(self.imgind!)")
            let imgview = UIImageView()
            imgview.backgroundColor = UIColor.black
            imgview.image = img
            imgview.frame = randompos
            imgview.transform = CGAffineTransform(rotationAngle: self.radius)
            starsview.addSubview(imgview)
            
        }
        
        self.timerlabel.isHidden = true
        
        datepicker.addTarget(self, action: #selector(self.settime), for: UIControl.Event.valueChanged)
        self.view.addSubview(blackscreen)
        blackscreen.isHidden = true
        self.view.bringSubviewToFront(self.timeview)
        
        
        
    }
    
    func randomPosition() {
        let height = self.view!.frame.height
        let width = self.view!.frame.width
        let ind = arc4random_uniform(4)
        let imgind = Int(arc4random_uniform(5))
        
        self.randompos = CGRect(
            x:CGFloat(arc4random()).truncatingRemainder(dividingBy: height),
            y: CGFloat(arc4random()).truncatingRemainder(dividingBy: width),
            width:CGFloat(20*(ind+1)),
            height:CGFloat(20*(ind+1)))
        
        
        let rotate = (Double.pi)/Double(ind)
        self.radius = CGFloat(rotate)
        self.imgind = imgind
    }
    
    @objc func settime(datePicker: UIDatePicker){
        tasktime = datePicker.countDownDuration
        
        let hours = Int(tasktime) / 3600
        let minutes = Int(tasktime) / 60 % 60
//        let seconds = Int(tasktime) % 60
        timerlabel.text = String(format:"%02i:%02i", hours, minutes)
        
        self.recordperiod = String(format:"%02i:%02i", hours, minutes)
        

    }
    
    @IBAction func starttask(_ sender: Any) {
        
        recordtime += 1
        self.view.bringSubviewToFront(blackscreen)
        
        // for test
        tasktime = 2
        
        if tasktime == nil{
            let alert = UIAlertController(title: "Message", message: "Please set task time", preferredStyle: .alert)
            let okaction = UIAlertAction(title: "OK", style: .default, handler: nil )
            alert.addAction(okaction)
            self.present(alert, animated: true, completion:nil)
            return
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countdown), userInfo: nil, repeats: true)
        checkstatetimer = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(bkgrdfail), userInfo: nil, repeats: true)
    }
    
    @objc func countdown(){
        
        tasktime -= 1
        let hours = Int(tasktime) / 3600
        let minutes = Int(tasktime) / 60 % 60
        timerlabel.text = String(format:"%02i:%02i", hours, minutes)
        if tasktime == 0 {
            self.success()
        }else{
            blackscreen.isHidden = false
            blackscreen.backgroundColor = UIColor.black
            blackscreen.frame = self.view.frame
            blackscreen.addTarget(self, action: #selector(touchfail), for: .touchDown)
            
        }
        
    }
    
    @IBAction func viewhistory(_ sender: Any) {
        records = UserDefaults.standard.array(forKey: "records") as? [String]
        performSegue(withIdentifier: "show", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationNavigationController = segue.destination as! UINavigationController
        let tvc = destinationNavigationController.topViewController as! TableController
        tvc.records = self.records
    }
    
    func success(){
        timer.invalidate()
        checkstatetimer.invalidate()
        blackscreen.isHidden = true
        
        let alert = UIAlertController(title: "Success", message: "Task Succeed!", preferredStyle: .alert)
        let okaction = UIAlertAction(title: "OK", style: .default, handler: {_ in self.successhandler()})
        alert.addAction(okaction)
        self.present(alert, animated: true, completion:nil)
        UserDefaults.standard.set(recordtime, forKey: "record")
        if self.records == nil {
            self.records = [self.recordperiod]
        } else {
            self.records.append(self.recordperiod)
        }
        UserDefaults.standard.set(self.records, forKey: "records")
        
        
    }
    
    func successhandler(){
        let imgs = UIImage(named: "3")
        let imgview = UIImageView()
        //        imgview.backgroundColor = UIColor.black
        imgview.image = imgs
        self.randomPosition()
        imgview.frame = randompos
        self.starsview.addSubview(imgview)
//        self.starsview.bringSubviewToFront(datepicker)
    }

    
    @objc func bkgrdfail(){
        let state = UIApplication.shared.applicationState
        if state != .active {
            timer.invalidate()
            checkstatetimer.invalidate()
            blackscreen.isHidden = true
            
        }
    }
    
    @objc func touchfail(){
        blackscreen.setTitle(self.timerlabel.text, for: .normal)
        blackscreen.titleLabel?.textColor = UIColor.white
        blackscreen.titleLabel?.font = .systemFont(ofSize: 50)
        blackscreen.contentVerticalAlignment = .bottom

        

        
        let alert = UIAlertController(title: "\(self.timerlabel.text!) LEFT", message: "Do you really want to quit?", preferredStyle: .alert)
        let okaction = UIAlertAction(title: "Quit", style: .default, handler: {_ in self.failhandlers()})
        let cancelaction = UIAlertAction(title: "Cancel", style: .default, handler: {
            _ in self.blackscreen.titleLabel?.isHidden = true
        })
        alert.addAction(okaction)
        alert.addAction(cancelaction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func failhandlers(){
        blackscreen.isHidden = true
        blackscreen.titleLabel?.isHidden = true
        timer.invalidate()
        checkstatetimer.invalidate()
    }
}

