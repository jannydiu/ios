//
//  File.swift
//  Notes
//
//  Created by Jan on 2019/4/9.
//  Copyright © 2019 Janny Wang. All rights reserved.
//

import Foundation

public class Note: NSObject,NSSecureCoding,Codable {
    
    public static var supportsSecureCoding: Bool = true
    
    public var title: String
    public var time: Date
    public var content: String
    
    public init?(title:String?,time:Date?,content:String?) {
        guard let title = title, let time = time, let content = content else { return nil }
        self.title = title
        self.time = time
        self.content = content
    }
    
    required convenience public init?(coder aDecoder:NSCoder) {
        let title = aDecoder.decodeObject(of: NSString.self, forKey:"title")
        let time = aDecoder.decodeObject(of: NSDate.self, forKey:"time")
        let content = aDecoder.decodeObject(of: NSString.self, forKey:"content")
        
        self.init(title: title as String?, time: time as Date?, content: content as String?)
        
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey:"title")
        aCoder.encode(time, forKey:"time")
        aCoder.encode(content, forKey:"content")
    }
    
}

extension Note {
    
    /// Loads weather forecast data from the App Group container.
    
    public static func loadSharedData() -> [ Note ] {
        // In case this is the first time the app has been run, create some initial data.
        if FileManager.default.fileExists(atPath: sharedDataFileURL.path) == false {
            saveSharedData(notes: [])
        }
        // Reload the data from disk
        do {
            let data = try Data(contentsOf:sharedDataFileURL)
            return try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [Note] ?? []
        } catch (let error) {
            fatalError(error.localizedDescription)
        }
    }

    public static func saveSharedData(notes:[Note]) -> Void {
        // save notes
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: notes, requiringSecureCoding:true)
            try data.write(to: sharedDataFileURL)
        } catch (let error) {
            print("Error saving notes to file \(error)")
        }
    }
    /// Returns the URL of a data file in the shared App Group container.
    /// - Tag: SharedAppGroupDirectory
    static var sharedDataFileURL: URL {
        let appGroupIdentifier = "group.com.example.apple-samplecode.NoteWidget"
        guard let url = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: appGroupIdentifier)
            else { preconditionFailure("Expected a valid app group container") }
        return url.appendingPathComponent("notes")
    }
    
}
