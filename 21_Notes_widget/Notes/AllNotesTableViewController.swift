//
//  AllNotesTableViewController.swift
//  Notes
//
//  Created by Janny Wang on 4/5/19.
//  Copyright © 2019 Janny Wang. All rights reserved.
//

import UIKit
import noteFrame

class AllNotesTableViewController: UITableViewController {

    var notes: [Note]!
    var url: URL!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.navigationItem.title = "Notes"
        let newBackButton = UIBarButtonItem(barButtonSystemItem:.add, target: self, action: #selector(addNote))
        self.navigationItem.rightBarButtonItem = newBackButton
        
        notes = Note.loadSharedData()
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notes = Note.loadSharedData()
        tableView.reloadData()
    }
    
    
    @objc func addNote() {
        // ref:https://learnappmaking.com/pass-data-between-view-controllers-swift-how-to/
        let nextVC = DetailedNoteViewController()
        nextVC.notes = notes
        navigationController?.pushViewController(nextVC, animated: true)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if notes != nil {
            return notes.count
        } else {
            return 0
        }
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "noteCellid", for: indexPath) as? NoteTableViewCell else {return UITableViewCell()}

        // Configure the cell...
        let note = notes[notes.count-1-indexPath.row]
        
        let time = note.time
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy HH:mm"
        cell.timeLabel.text = formatter.string(from: time)
        cell.contentLabel.text = note.title
        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            notes.remove(at: notes.count-1-indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
            
            // save data to file
            Note.saveSharedData(notes: notes)
            
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        guard let detailVC = segue.destination as? DetailedNoteViewController else { return }
        guard let selectedIndexPath = tableView.indexPathForSelectedRow else { return }
        let note = notes[notes.count - 1 - selectedIndexPath.row]
        // ref: https://stackoverflow.com/questions/24051633/how-to-remove-an-element-from-an-array-in-swift
        notes.remove(at: notes.count - 1 - selectedIndexPath.row)
        detailVC.text = note.content
        detailVC.notes = notes

    }
    
    

}
