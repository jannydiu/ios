//
//  DetailedNoteViewController.swift
//  Notes
//
//  Created by Janny Wang on 4/5/19.
//  Copyright © 2019 Janny Wang. All rights reserved.
//

import UIKit
import noteFrame

class DetailedNoteViewController: UIViewController {
    
    var textView: UITextView!
    var text: String = ""
    var note: Note = Note(title: "", time: Date(), content: "")!
    var notes: [Note]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView = UITextView()
        textView.frame = self.view.frame
        textView.text = text
        textView.font = UIFont.preferredFont(forTextStyle: .body)
        self.view.addSubview(textView)
        self.view.bringSubviewToFront(textView)
        
        //ref: https://stackoverflow.com/questions/27713747/execute-action-when-back-bar-button-of-uinavigationcontroller-is-pressed
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(saveNote))
        self.self.navigationItem.leftBarButtonItem = newBackButton

        //self.navigationItem.backBarButtonItem?.action = #selector(saveNote)
        //self.navigationItem.leftBarButtonItem = newBackButton
        
        // Do any additional setup after loading the view.
    }
    
    @objc func saveNote() {
        
        // store title
        // ref: https://stackoverflow.com/questions/31844784/creating-title-from-first-line-of-uitextview-think-apple-notes-app-swift
        let length = self.textView.text.count
        if length > 10 {
            let firstLine = (textView.text as NSString).substring(to: 10)
            note.title = firstLine
        } else {
            let firstLine = (textView.text as NSString).substring(to: length)
            note.title = firstLine
        }
        // store time
        let time = Date()
        note.time = time
        // store content
        note.content = textView.text
        if notes == nil {
            notes = [note]
        } else {
            notes.append(note)
        }
        Note.saveSharedData(notes: notes)
        self.navigationController?.popViewController(animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
