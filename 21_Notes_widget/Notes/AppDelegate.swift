//
//  AppDelegate.swift
//  Notes
//
//  Created by Janny Wang on 4/5/19.
//  Copyright © 2019 Janny Wang. All rights reserved.
//

import UIKit
import noteFrame

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        var notes = Note.loadSharedData()

        /// - Tag: ParseIncomingURL
        // Extract the daysFromNow query string parameter from the incoming URL.
        let components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        guard let daysFromNowQueryItem = components?.queryItems?.first(where: { queryItem -> Bool in
            return queryItem.name == "daysFromNow"
        }) else { preconditionFailure("Expected a daysFromNow parameter") }
        guard let daysFromNowValue = daysFromNowQueryItem.value
            else { preconditionFailure("Expected daysFromNow parameter to have a value") }
        guard let daysFromNow = Int(daysFromNowValue)
            else { preconditionFailure("Expected daysFromNow to be an integer") }
        
        guard let navigationController = window?.rootViewController as? UINavigationController
            else { preconditionFailure("Expected the root view controller to be a UINavigationController") }
        let nextVC = DetailedNoteViewController()
        
        // remove current note from notes
        if notes != [] {
            let note = notes[notes.count-1-daysFromNow]
            notes.remove(at: notes.count-1-daysFromNow)
            nextVC.text = note.content
        }
        nextVC.notes = notes
        navigationController.pushViewController(nextVC, animated: true)
        
        return true
    }
}

