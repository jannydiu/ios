//
//  noteTableViewCell.swift
//  noteExtension
//
//  Created by Janny Wang on 4/9/19.
//  Copyright © 2019 Janny Wang. All rights reserved.
//

import UIKit

class noteTableViewCell: UITableViewCell {

    
    /// The reuse identifier for this table view cell.
    static let reuseIdentifier = "noteTableViewCell"

    // Heights for the two styles of cell display.
    static let todayCellHeight: CGFloat = 110
    static let standardCellHeight: CGFloat = 90
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
}
