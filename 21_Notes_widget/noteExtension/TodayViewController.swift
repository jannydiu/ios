//
//  TodayViewController.swift
//  noteExtension
//
//  Created by Janny Wang on 4/9/19.
//  Copyright © 2019 Janny Wang. All rights reserved.
//

import UIKit
import NotificationCenter
import noteFrame


class ForecastExtensionViewController: UITableViewController, NCWidgetProviding {
    
    var notes = Note.loadSharedData()
    var url: URL!
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Allow the today widget to be expanded or contracted.
        if notes != [] {
                    extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        } else {
            extensionContext?.widgetLargestAvailableDisplayMode = .compact
        }

        // Register the table view cell.
        let noteTableViewCellNib = UINib(nibName: "noteTableViewCell", bundle: nil)
        tableView.register(noteTableViewCellNib, forCellReuseIdentifier: "noteTableViewCell")
        //tableView.reloadData()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notes = Note.loadSharedData()
        tableView.reloadData()
        
    }
    
    // MARK: - Widget provider protocol
    
    /// - Tag: WidgetPerformUpdate
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Reload the data from disk
        notes = Note.loadSharedData()
        completionHandler(NCUpdateResult.newData)
    }
   
    /// - Tag: ActiveDisplayModeDidChange
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        switch activeDisplayMode {
        case .compact:
            // The compact view is a fixed size.
            preferredContentSize = maxSize
        case .expanded:
            // Dynamically calculate the height of the cells for the extended height.
            var height: CGFloat = 0
            if notes != [] {
                if (notes.count < 3) {
                    for _ in 0 ... notes.count-1 {
                        height += noteTableViewCell.standardCellHeight
                    }
                } else {
                    for _ in 0...2 {
                        height += noteTableViewCell.standardCellHeight
                    }
                }
            } else {
                extensionContext?.widgetLargestAvailableDisplayMode = .compact
            }
            preferredContentSize = CGSize(width: maxSize.width, height: min(height, maxSize.height))
            tableView.reloadData()
        }
    }
    
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfTableRowsToDisplay()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: noteTableViewCell.reuseIdentifier,
                                                       for: indexPath) as? noteTableViewCell
            else { preconditionFailure("Expected to dequeue a ForecastWidgetCell") }
        let label = UILabel(frame: CGRect(x: cell.frame.minX, y: cell.frame.minY, width: cell.frame.width, height: cell.frame.height))
        label.text = "No notes"
        label.font = UIFont(descriptor: .preferredFontDescriptor(withTextStyle: .body), size: 50)
        label.textAlignment = .center
        
        if notes != [] {
            let note = notes[notes.count-1-indexPath.row]
            label.removeFromSuperview()
            cell.titleLabel.text = note.title
            let time = note.time
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm"
            cell.timeLabel.text = formatter.string(from: time)
            cell.timeLabel.text = formatter.string(from: time)
        } else {
            cell.timeLabel.isHidden = true
            cell.titleLabel.isHidden = true
            cell.addSubview(label)
            cell.bringSubviewToFront(label)
            extensionContext?.widgetLargestAvailableDisplayMode = .compact
        }

        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0: return noteTableViewCell.todayCellHeight
        default: return noteTableViewCell.standardCellHeight
        }
    }
    
    /// - Tag: OpenMainApp
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Open the main app at the correct page for the day tapped in the widget.
        //let note = notes[notes.count-1-indexPath.row]
        if let appURL = URL(string: "notewidget://?daysFromNow=\(indexPath.row)") {
            extensionContext?.open(appURL, completionHandler: nil)
        }
        // Don't leave the today extension with a selected row.
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Helpers
    func numberOfTableRowsToDisplay() -> Int {
        if extensionContext?.widgetActiveDisplayMode == NCWidgetDisplayMode.compact {
            print("comp")
            return 1
        } else {
            print("exp")
            return min(notes.count,3)
        }
    }
 
}
