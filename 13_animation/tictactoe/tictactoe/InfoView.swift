//
//  InfoView.swift
//  tictactoe
//
//  Created by Jan on 2019/2/7.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class InfoView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 20
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 5
        layer.masksToBounds = true
        
    }

    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let width = rect.width
        let height = rect.height

        let line = UIBezierPath()
        // horizontal line
        line.move(to: CGPoint(x:10,y:2*height/3))
        line.addLine(to: CGPoint(x: width-10, y: 2*height/3))
        //draw line
        line.lineWidth = 5
        UIColor.white.setStroke()
        line.stroke()
    }
    
 



    
}
