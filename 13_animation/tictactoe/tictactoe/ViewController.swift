//
//  ViewController.swift
//  tictactoe
//
//  Created by Jan on 2019/2/6.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var gridview: GridView!
    
    @IBOutlet var Squares: [UIView]!
    
    
    var squares = [0,0,0,0,0,0,0,0,0]
    
    @IBOutlet weak var infoview: InfoView!
    
    @IBOutlet weak var infoviewlabel: UILabel!
    
    
    @IBOutlet weak var xview: UILabel!
    
    @IBOutlet weak var oview: UILabel!
    
    var originxframe: CGPoint?
    var originoframe: CGPoint?
    let grid: Grid = Grid()
    var over = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.infoview.alpha = 0
        originxframe = xview.frame.origin
        originoframe = oview.frame.origin
        self.start(another: xview)
    }

    
    @IBAction func inforaction(_ sender: Any) {
        infoviewlabel.text = "Get 3 in a row to win!"
        self.infoview.alpha = 1
        self.view.bringSubviewToFront(infoview)
        let enter = UIViewPropertyAnimator(
            duration: 1,
            curve: .linear) {
                // fall to center
                self.infoview.center = CGPoint(x: self.view.center.x, y: self.view.center.y-self.infoview.frame.height/2)
                
        }
        enter.startAnimation()
    }
    
    @IBAction func okaction(_ sender: Any) {
        let out = UIViewPropertyAnimator(
            duration: 1,
            curve: .linear) {
                self.infoview.frame.origin = CGPoint(x: self.infoview.frame.origin.x, y: self.view.frame.maxY)
                self.infoview.alpha = 0
        }
        let back  = UIViewPropertyAnimator(
            duration: 0,
            curve: .linear) {
                self.infoview.frame.origin = CGPoint(x: self.infoview.frame.origin.x, y: self.view.frame.minY)
        }
        out.addCompletion { (_) in
            back.startAnimation()
        }
        out.startAnimation()
        
        // clear the board
        if self.over {
                let up = self.gridview.frame.minY
                let down = self.gridview.frame.maxY
            var labels = [UIView]()
            for v in self.view.subviews{
                if ((v is UILabel) && (v.frame.minY >= up) && (v.frame.maxY <= down)){
                    labels.append(v)
                }
            }
            // reference : https://stackoverflow.com/questions/32663327/multiple-animations-on-different-objects-at-the-same-time-with-a-final-completio
            UIView.animate(withDuration: 1, animations: {
                for v in labels {
                    v.alpha = 0}},
                    completion:{ _ in
                    for v in labels{
                        v.removeFromSuperview()
                    }}
                )
            
            // start new game from x
                self.start(another: xview)
                self.squares = [0,0,0,0,0,0,0,0,0]
                self.over = false
        }
    }


    @IBAction func xmove(_ sender: UIPanGestureRecognizer) {
        guard let piece = sender.view else {
            return
        }
        let anotherpiece = oview
        let translation = sender.translation(in: piece)
        if sender.state == .changed{
            piece.frame.origin.x += translation.x
            piece.frame.origin.y += translation.y
        }
        if sender.state == .ended{
            // if x is out of the grid, put it back
            if piece.frame.intersects(gridview.frame){
                for i in 0...8{
                    let square = Squares[i]
                    // check intersection
                    if piece.frame.intersects(square.frame){
                        // check occupation
                        if grid.checkstate(squares: squares, index: i) == "available"{
                            squares[i] = 1
                            let animatefit = UIViewPropertyAnimator(duration: 0.5, curve: .linear) {
                                piece.frame.origin = square.frame.origin
                            }
                            animatefit.addCompletion { (_) in
                                self.end(viewforCopy: piece,position: self.originxframe!, current: "x")
                            }
                            animatefit.startAnimation()
                            // check if game over
                            let response = grid.gameover(squares: squares)
                            // if game is not over
                            if response == "not over"{
                                self.start(another: anotherpiece!)}else{
                            // if game is over
                                
                                
                                // draw a line
                                self.gridview.layer.sublayers = nil
                                let line = UIBezierPath()
                                

                                let st:CGPoint!
                                let ed:CGPoint!
                                
                                switch [1,1,1]{
                                case [squares[0],squares[1],squares[2]]:
                                    st = CGPoint(x: self.Squares[0].frame.midX-16, y: self.Squares[0].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[2].frame.midX-16, y: self.Squares[2].frame.midY-136)
                                case [squares[3],squares[4],squares[5]]:
                                    st = CGPoint(x: self.Squares[3].frame.midX-16, y: self.Squares[3].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[5].frame.midX-16, y: self.Squares[5].frame.midY-136)
                                case [squares[6],squares[7],squares[8]]:
                                    st = CGPoint(x: self.Squares[6].frame.midX-16, y: self.Squares[6].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[8].frame.midX-16, y: self.Squares[8].frame.midY-136)
                                case [squares[0],squares[3],squares[6]]:
                                    st = CGPoint(x: self.Squares[0].frame.midX-16, y: self.Squares[0].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[6].frame.midX-16, y: self.Squares[6].frame.midY-136)
                                case [squares[1],squares[4],squares[7]]:
                                    st = CGPoint(x: self.Squares[1].frame.midX-16, y: self.Squares[1].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[7].frame.midX-16, y: self.Squares[7].frame.midY-136)
                                case [squares[2],squares[5],squares[8]]:
                                    st = CGPoint(x: self.Squares[2].frame.midX-16, y: self.Squares[2].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[8].frame.midX-16, y: self.Squares[8].frame.midY-136)
                                case [squares[0],squares[4],squares[8]]:
                                    st = CGPoint(x: self.Squares[0].frame.midX-16, y: self.Squares[0].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[8].frame.midX-16, y: self.Squares[8].frame.midY-136)
                                case [squares[2],squares[4],squares[6]]:
                                    st = CGPoint(x: self.Squares[2].frame.midX-16, y: self.Squares[2].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[6].frame.midX-16, y: self.Squares[6].frame.midY-136)
                                default:
                                    st = CGPoint(x:0,y:0)
                                    ed = CGPoint(x:0,y:0)
                                }
                                
                                if st != ed{
                                    line.move(to: st)
                                    line.addLine(to: ed)
                                    
                                    let shapeLayer = CAShapeLayer()
                                    shapeLayer.path = line.cgPath
                                    shapeLayer.strokeColor = UIColor.blue.cgColor
                                    shapeLayer.lineWidth = 12
                                    shapeLayer.lineCap = .round
                                    let strokeAnimation = CABasicAnimation(keyPath: #keyPath(CAShapeLayer.strokeEnd))
                                    strokeAnimation.duration = CFTimeInterval(1)
                                    self.gridview.layer.addSublayer(shapeLayer)
                                    shapeLayer.bringToFront()
                                    
                                    
                                    
                                    Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false) { (_) in
                                        let transition = CATransition()
                                        transition.duration = CFTimeInterval(0.5)
                                        //
                                        transition.type = CATransitionType.fade
                                        transition.subtype = CATransitionSubtype.fromTop
                                        
                                        shapeLayer.add(transition,forKey: nil)
                                        shapeLayer.opacity = 0
                                        
                                    }
                                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
                                        shapeLayer.removeFromSuperlayer()
                                    }
                                }
                                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
                                     //infoview pop out
                                    self.infoviewlabel.text = "Congratulations, "+response
                                    self.over = true
                                    // in
                                    self.infoview.alpha = 1
                                    self.view.bringSubviewToFront(self.infoview)
                                    let enter = UIViewPropertyAnimator(
                                        duration: 1,
                                        curve: .linear) {
                                            // fall to center
                                            self.infoview.center = CGPoint(x: self.view.center.x, y: self.view.center.y-self.infoview.frame.height/2)}
                                    enter.startAnimation()
                                }
                            }}
                        else{
                            UIView.animate(
                            withDuration: 0.5) {
                                piece.frame.origin = self.originxframe!
                            }
                        }
                    }}
            }else{
                UIView.animate(
                withDuration: 0.5) {
                    piece.frame.origin = self.originxframe!
                }
                
            }}
        sender.setTranslation(CGPoint(x:0,y:0), in: view)
    }
    
    
    @IBAction func omove(_ sender: UIPanGestureRecognizer) {
        guard let piece = sender.view else {
            return
        }
        let anotherpiece = xview
        let translation = sender.translation(in: piece)
        if sender.state == .changed{
            piece.frame.origin.x += translation.x
            piece.frame.origin.y += translation.y
        }
        if sender.state == .ended{
            // if o is out of the grid, put it back
            if piece.frame.intersects(gridview.frame){
                for i in 0...8{
                    let square = Squares[i]
                    // check intersection
                    if piece.frame.intersects(square.frame){
                        // check occupation
                        if grid.checkstate(squares: squares, index: i) == "available"{
                            squares[i] = 2
                            let animatefit = UIViewPropertyAnimator(duration: 0.5, curve: .linear) {
                                piece.frame.origin = square.frame.origin
                            }
                            animatefit.addCompletion { (_) in
                                self.end(viewforCopy: piece,position: self.originoframe!, current:"o")
                            }
                            animatefit.startAnimation()
                            // check if game over
                            let response = grid.gameover(squares: squares)
                            if response=="not over"{
                                // if game is not over
                                self.start(another: anotherpiece!)
                            }else{
                                self.over = true
                                // if game is over
                                // draw a line
                                self.gridview.layer.sublayers = nil
                                let line = UIBezierPath()
                                
                                
                                let st:CGPoint!
                                let ed:CGPoint!
                                
                                switch [2,2,2]{
                                case [squares[0],squares[1],squares[2]]:
                                    st = CGPoint(x: self.Squares[0].frame.midX-16, y: self.Squares[0].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[2].frame.midX-16, y: self.Squares[2].frame.midY-136)
                                case [squares[3],squares[4],squares[5]]:
                                    st = CGPoint(x: self.Squares[3].frame.midX-16, y: self.Squares[3].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[5].frame.midX-16, y: self.Squares[5].frame.midY-136)
                                case [squares[6],squares[7],squares[8]]:
                                    st = CGPoint(x: self.Squares[6].frame.midX-16, y: self.Squares[6].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[8].frame.midX-16, y: self.Squares[8].frame.midY-136)
                                case [squares[0],squares[3],squares[6]]:
                                    st = CGPoint(x: self.Squares[0].frame.midX-16, y: self.Squares[0].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[6].frame.midX-16, y: self.Squares[6].frame.midY-136)
                                case [squares[1],squares[4],squares[7]]:
                                    st = CGPoint(x: self.Squares[1].frame.midX-16, y: self.Squares[1].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[7].frame.midX-16, y: self.Squares[7].frame.midY-136)
                                case [squares[2],squares[5],squares[8]]:
                                    st = CGPoint(x: self.Squares[2].frame.midX-16, y: self.Squares[2].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[8].frame.midX-16, y: self.Squares[8].frame.midY-136)
                                case [squares[0],squares[4],squares[8]]:
                                    st = CGPoint(x: self.Squares[0].frame.midX-16, y: self.Squares[0].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[8].frame.midX-16, y: self.Squares[8].frame.midY-136)
                                case [squares[2],squares[4],squares[6]]:
                                    st = CGPoint(x: self.Squares[2].frame.midX-16, y: self.Squares[2].frame.midY-136)
                                    ed = CGPoint(x: self.Squares[6].frame.midX-16, y: self.Squares[6].frame.midY-136)
                                default:
                                    st = CGPoint(x:0,y:0)
                                    ed = CGPoint(x:0,y:0)
                                }
                                
                                if st != ed{
                                    line.move(to: st)
                                    line.addLine(to: ed)
                                    
                                    let shapeLayer = CAShapeLayer()
                                    shapeLayer.path = line.cgPath
                                    shapeLayer.strokeColor = UIColor.blue.cgColor
                                    shapeLayer.lineWidth = 12
                                    shapeLayer.lineCap = .round
                                    let strokeAnimation = CABasicAnimation(keyPath: #keyPath(CAShapeLayer.strokeEnd))
                                    strokeAnimation.duration = CFTimeInterval(1)
                                    self.gridview.layer.addSublayer(shapeLayer)
                                    shapeLayer.bringToFront()
                                    
                                    
                                    
                                    Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false) { (_) in
                                        let transition = CATransition()
                                        transition.duration = CFTimeInterval(0.5)
                                        //
                                        transition.type = CATransitionType.fade
                                        transition.subtype = CATransitionSubtype.fromTop
                                        
                                        shapeLayer.add(transition,forKey: nil)
                                        shapeLayer.opacity = 0
                                        
                                    }
                                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
                                        shapeLayer.removeFromSuperlayer()
                                    }
                                }
                                
                                
                                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
                                    //infoview pop out
                                    self.infoviewlabel.text = "Congratulations, "+response
                                    self.over = true
                                    // in
                                    self.infoview.alpha = 1
                                    self.view.bringSubviewToFront(self.infoview)
                                    let enter = UIViewPropertyAnimator(
                                        duration: 1,
                                        curve: .linear) {
                                            // fall to center
                                            self.infoview.center = CGPoint(x: self.view.center.x, y: self.view.center.y-self.infoview.frame.height/2)}
                                    enter.startAnimation()
                                }
                            }
                        }}
                }}else{
                UIView.animate(
                withDuration: 0.5) {
                    piece.frame.origin = self.originoframe!
                }
                
            }}
        sender.setTranslation(CGPoint(x:0,y:0), in: view)
    }
    
    
    func start(another:UIView){
        // set alpha
        another.alpha = 1
        // swing animation
        let attention1 = UIViewPropertyAnimator(duration: 0.2, curve: .linear) {
            another.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/6)
        }
        let attention2 = UIViewPropertyAnimator(duration: 0.2, curve: .linear) {
            another.transform = CGAffineTransform(rotationAngle: CGFloat.pi/6)
        }
        let attention3 = UIViewPropertyAnimator(duration: 0.2, curve: .linear) {
            another.transform = CGAffineTransform(rotationAngle: 0)
        }
        attention1.addCompletion { (_) in
            attention2.startAnimation()
        }
        attention2.addCompletion { (_) in
            attention3.startAnimation()
        }
        attention1.startAnimation()
        // enable user interation
        another.isUserInteractionEnabled = true
    }
    
    func end(viewforCopy:UIView,position:CGPoint,current:String){
        // copy the view and place it at the original position
        var newview: UILabel!
        // ref: https://stackoverflow.com/questions/27121655/create-a-copy-of-a-uiview-in-swift
        do{
        let data = try NSKeyedArchiver.archivedData(withRootObject: viewforCopy, requiringSecureCoding: false)
            newview = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? UILabel}catch{
                newview = UILabel.init()
        }
        newview.frame.origin = position
        self.view.addSubview(newview)
        // add gesture
        if current == "x"{
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.xmove(_:)))
            newview.addGestureRecognizer(panGesture)
            self.xview = newview}
        else{
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.omove(_:)))
            newview.addGestureRecognizer(panGesture)
            self.oview = newview}
        // set alpha
        newview.alpha = 0.5
        // disable user interaction
        viewforCopy.isUserInteractionEnabled = false
        newview.isUserInteractionEnabled = false
    }
    

}

//ref: https://stackoverflow.com/questions/6547527/ios-core-animation-calayer-bringsublayertofront
extension CALayer {
    
    func bringToFront() {
        guard let sLayer = superlayer else {
            return
        }
        removeFromSuperlayer()
        sLayer.insertSublayer(self, at: UInt32(sLayer.sublayers?.count ?? 0))
    }
    
    func sendToBack() {
        guard let sLayer = superlayer else {
            return
        }
        removeFromSuperlayer()
        sLayer.insertSublayer(self, at: 0)
    }
}
