//
//  Grid.swift
//  tictactoe
//
//  Created by Jan on 2019/2/9.
//  Copyright © 2019 Jan. All rights reserved.
//

import UIKit

class Grid: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    // to keep track of squares
    func checkstate(squares:Array<Int>,index:Int)->String{
        if squares[index]==1{
            return "x occupied"
        }else if squares[index]==2{
            return "o occupied"
        }else{
            return "available"
        }
    }
    
    // determine when to declare a winner
    func gameover(squares:Array<Int>) -> String {
        let combs = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
        var counter = 0
        for comb in combs{
            var res = [Int]()
            for i in comb{
                res.append(squares[i])
            }
            if res == [1,1,1]{
                return "X wins!"
            }else if res==[2,2,2]{
                return "O wins!"
            }
            if !res.contains(0){
                counter += 1
            }
        }
        if counter == 8{
            return "Tie!"
        }else{
            return "not over"
        }
    }
}
